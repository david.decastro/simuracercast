# !/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title       : json_data.py
# Author      : aDd1kTeD2Ka0s (add1kted2ka0s@gmail.com)
# Date        : 27-Jul-2014
# Description : Ka0s Lap Distance - JSON Data
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------

import ac
import codecs
import json
import os

# -------------------------------------
class JsonData:
	# -------------------------------------
	_json_data = None

	# -------------------------------------
	def __init__(self, type, key, key2): # type = u'cars' or u'tracks'
		# -------------------------------------
		try:
			if type == u'cars':
				filename = u'ui_car.json'
			elif type == u'tracks':
				filename = u'ui_track.json'
			else:
				return
			if key2 == "":
				json_file_path = os.path.join(os.getcwd(), u'content', type, key, u'ui', filename)
			else:
				json_file_path = os.path.join(os.getcwd(), u'content', type, key, u'ui', key2, filename)
			with codecs.open(json_file_path, "rb", encoding="windows-1252") as json_file:
				raw_data = json_file.read().replace('\n', '').replace('\r', '').replace('\t', '')
			self._json_data = json.loads(raw_data)
		except Exception as e:
			ac.log('Error opening/parsing file : %s => %s ' % (json_file_path, str(e)))

	# -------------------------------------
	def get(self, key):
		# -------------------------------------
		if self._json_data and key in self._json_data:
			return self._json_data[key]
		else:
			return None
