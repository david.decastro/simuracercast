#
# SimuRacerCast with persistent config settings. Edited
# by davisuke.noara@gmail.com and deiff@dasonu.com.
# Original version available here:
# http://www.assettocorsa.net/forum/index.php?threads/32536/
#
# VERSION 1.3.2
#

import ac
import acsys
import sys
import os
import os.path
import platform
import threading
import urllib
from operator import attrgetter

# import libraries
try:
    if platform.architecture()[0] == "64bit":
        sys.path.insert(0, os.path.dirname(__file__)+"/dll64")
    else:
        sys.path.insert(0, os.path.dirname(__file__)+"/dll")
    sys.path.insert(0, os.path.dirname(__file__)+"/Libraries")
    import time
    import math
    import configparser
    from random import randint
    from collections import OrderedDict
    from third_party.sim_info import info
    from third_party.json_data import JsonData
    #URL requests for scoreTableServers
    import requests
    from collections import defaultdict
except Exception as e:
    ac_log("Error importing libraries: %s" % e)
    raise

# En principio no hace falta esto: os.environ['PATH'] = os.environ['PATH'] + ";.;"+os.path.abspath(os.path.dirname(__file__))+"\Libraries"

#
# Formatted logging
#
log_head = "SimuRacerCast"
def ac_log(msg):
    """
    Writes a formated log line to py_log.txt in AC's documents folder
    """
    ac.log("[%s %s] %s" % (log_head, time.strftime("%H:%M:%S", time.localtime()), msg))


#
# Persistence via config file (ini)
#
try:
    configfile = os.path.join(os.path.dirname(__file__), 'SimuRacerCast.ini')
    if not os.path.exists(configfile):
        ac_log("Creating new SimuRacerCast.ini file")
        open(configfile,'w').close()

    config = configparser.ConfigParser()
    config.read(configfile)
except Exception as e:
    ac_log("Exception is: %s" % e)
    config = None

def updateIni(section, item, value):
    """
    Updates the ini file if the value given differs from the one in the file
    """
    global config, configfile

    # If config is None, then reading the config file failed.
    # Don't try to do anything
    if config is None:
        return

    # Config files like strings, not ints or lists, etc.  Make sure we're
    # dealing with strings only
    if not isinstance(value, str):
        value = str(value)

    try:
        ini_val = config[section][item]
    except:
        ini_val = None

    # Don't try to update the file if there's no need
    if value == ini_val:
        return

    ac_log("Updating ini file '%s'/'%s': %s" % (section, item, value))

    if not config.has_section(section):
        config.add_section(section)

    config[section][item] = value
    try:
        config.write(open(configfile, 'w'))
    except Exception as e:
        ac_log("WARNING: Could not write to ini file '%s': %s" % (configfile, e))


# Para posicionar las etiquetas usando las teclas WASD
# Hay que descomentar todas las líneas que aparezca
# tanto xTest e yTest, como xlblTest
xTest=1.0
yTest=1.0

# Standings
driverDisplayedGlobal = 25

trackLength = 0
lastUpdateTime = 0
lastTimeFastestLapUpdate = 0.0
currentId = 0
lastCamFocusSwitchTime = 0
lastCamTypeChangeTime = 0
camFocusHold = 0
AdWindowVisible = False
SubtitleWindowVisible = True
SessionInfoWindowVisible = True
StandingWindowVisible = True
ScoreTableWindowVisible = False

StandingWindowTypes = ["gap", "head_gap", "maxSpeed"]
currentStandingWindowType = 0

SubtitleWindowTypes = ["currentLap", "lastLap"]
currentSubtitleWindowType = 0

appStartTime = 0
session_prev = 0

# Last time when a driver has been removed
driverRemovedTime = 0

# Best times for each sector of the session
bestRaceSector1 = 99999999
bestRaceSector2 = 99999999
bestRaceSector3 = 99999999

# Pilot who made the best time in each sector
bestRaceSector1Car = ""
bestRaceSector2Car = ""
bestRaceSector3Car = ""

# Best race lap
bestRaceLapTime = 99999999
bestRaceLapName = ""
lastBestRaceLapTime = 99999999
bestRaceLapSlotId = 0

# Timestamp to fix position change on finish line
lastFinishLineTime = 0

# Objects
SimuRacerCast = {
    "subtitle": 0,
    "controller": 0,
    "session_info": 0,
    "standing": 0,
    "ad": 0
}

cars = []
bgpath_pos = None
bgpath_name = None
bgpath_tyre = None
bgpath_info = None
bgpath_info_pit = None
bgpath_pit = None
bgpath_nopit = None
bgpath_pits_n = None
bgpath_info_ot = None
bgpath_checkered = None
bgpath_session_info = None
bgpath_blank = None
bgpath_position_up = None
bgpath_position_down = None
bgpath_position_equal = None
bgpath_fastest_lap = None
bgpath_score_table_title = None
bgpath_sector_normal = None
bgpath_sector_pb = None
bgpath_sector_sb = None
bgpath_subtitle = None
bgpath_subtitle_last_lap = None
bgpath_subtitle_pits = None
nextMostInterestingCar = None
textureId_ad = None

## CONFIG PARAMETERS

try:
    sizeSessionInfo = config.getint("Options", "sizeSessionInfo")
except:
    sizeSessionInfo = 10
    updateIni("Options", "sizeSessionInfo", str(sizeSessionInfo).lower())

try:
    sizeStanding = config.getint("Options", "sizeStanding")
except:
    sizeStanding = 10
    updateIni("Options", "sizeStanding", str(sizeStanding).lower())

try:
    sizeSubtitle = config.getint("Options", "sizeSubtitle")
except:
    sizeSubtitle = 10
    updateIni("Options", "sizeSubtitle", str(sizeSubtitle).lower())

try:
    sizeController = config.getint("Options", "sizeController")
except:
    sizeController = 10
    updateIni("Options", "sizeController", str(sizeController).lower())

try:
    sizeFastestLap = config.getint("Options", "sizeFastestLap")
except:
    sizeFastestLap = 10
    updateIni("Options", "sizeFastestLap", str(sizeFastestLap).lower())

# Get the commenters' name
try:
    skipDrivers = list(map(lambda name: name.strip().lower(), config.get("Options", "skipDrivers").split("|")))
except:
    skipDrivers = ["fulanito de tal"]

# Get all score tables from ScoreTableServers splitted by comma
try:
    scoreTableServers = config["Options"]["scoreTableServers"].split('|')
    if scoreTableServers[0] == "":
        scoreTableServers = []
except:
    scoreTableServers = []

# Get the score table size
try:
    sizeScoreTable = config.getint("Options", "sizeScoreTable")
except:
    sizeScoreTable = 10
    updateIni("Options", "sizeScoreTable", str(sizeScoreTable).lower())

# Points according to race position
try:
    racePoints = [int(x) for x in config["Options"]["racePoints"].split(",")]
except:
    racePoints = [75, 68, 64, 60, 57, 54, 51, 48, 45, 42, 39, 36, 33, 30, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1]
# Points according to qualy position
try:
    qualyPoints = [int(x) for x in config["Options"]["qualyPoints"].split(",")]
except:
    qualyPoints = [3, 2, 1]
# Points per fast lap
try:
    fastestLapPoints = int(config["Options"]["fastestLapPoints"])
except:
    fastestLapPoints = 1

updateTime = 20 #miliseconds
minCamSwitchTime = 10 #seconds. It doesn't switch camera at least for this amount of time.
configEndOfSectorDelay = 30 # delay = thisvalue * (#cars / fps) seconds
minCamTypeChangeTime = 10 # seconds
theme = "SimuRacer"
debug = False
maxDriverCount = 32
showStandingDelta = 1
showStandingBorder = 0
opacityStanding = 0
opacityScoreTable = 0
unit = "time"
SC = "mercedes_sls_sc"
fontname = "Arial Unicode MS"
time_in_battle = 0.3
position_change_duration = 120

try:
    keepHidingFastestLap = config.getint("Options", "keepHidingFastestLap")
except:
    keepHidingFastestLap = 0
    updateIni("Options", "keepHidingFastestLap", str(keepHidingFastestLap))

try:
    hideDriverPhoto = config.getint("Options", "hideDriverPhoto")
except:
    hideDriverPhoto = 0
    updateIni("Options", "hideDriverPhoto", str(hideDriverPhoto))

try:
    isWECRace = config.getint("Options", "isWECRace")
except:
    isWECRace = 0
    updateIni("Options", "isWECRace", str(isWECRace))

try:
    WECEndpoint = config.get("Options", "WECEndpoint")
except:
    WECEndpoint = "http://simuwec.dykykyd.com:80/nohttps/getCurrentRacerName.php?teamName="
    updateIni("Options", "WECEndpoint", str(WECEndpoint))

try:
    autoSwitch = config.getboolean("Options", "autoSwitch")
except:
    autoSwitch = True
    updateIni("Options", "autoSwitch", str(autoSwitch))

try:
    selectedCameras = config["Options"]["selectedCameras"].split(',')
except:
    selectedCameras = [] #"Track",]
    updateIni("Options", "selectedCameras", ",".join(selectedCameras))

# autoSwitch = False
# selectedCameras = []

# selCamCockpit=False
# selCamCar=False
# selCamDrivable=False
# selCamTrack=False
# selCamHelicopter=False
# selCamOnBoardFree=False
# selCamFree=False
# selCamImageGeneratorCamera=False
# selCamStart=False


##############################################################
## MAIN FUNCTION
##############################################################
def acMain(ac_version):
    try:
        global trackLength
        global theme
        global driverDisplayedGlobal
        global maxDriverCount
        global bgpath_pos, bgpath_name, bgpath_info,  bgpath_session_info, bgpath_subtitle, bgpath_subtitle_last_lap, bgpath_pit, bgpath_nopit, bgpath_pits_n, bgpath_info_ot, bgpath_checkered, bgpath_tyre, bgpath_blank, bgpath_position_up, bgpath_position_down, bgpath_position_equal, bgpath_fastest_lap, bgpath_score_table_title, bgpath_sector_normal, bgpath_sector_pb, bgpath_sector_sb, bgpath_subtitle_pits
        global appStartTime
        global textureId_ad

        # Load UI graphics
        bgpath_pos = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_pos.png'
        bgpath_name = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_name.png'
        bgpath_tyre = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_tyre.png'
        bgpath_info = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_info.png'
        bgpath_nopit = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_nopit.png'
        bgpath_pit = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_pit.png'
        bgpath_pits_n = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_pits_number.png'
        bgpath_info_ot = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_info_arrow_up.png'
        bgpath_checkered = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_checkered.png'
        bgpath_session_info = os.path.dirname(__file__) + '/img/' + theme + '/session_info/bg_session_info.png'
        bgpath_subtitle = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_subtitle.png'
        bgpath_subtitle_last_lap = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_subtitle_last_lap.png'
        bgpath_fastest_lap = os.path.dirname(__file__) + '/img/' + theme + '/fastest_lap/bg_fastest_lap.png'
        bgpath_score_table_title = os.path.dirname(__file__) + '/img/' + theme + '/score_table/bg_score_table_title.png'
        bgpath_sector_normal = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_sector_normal.png'
        bgpath_sector_pb = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_sector_pb.png'
        bgpath_sector_sb = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_sector_sb.png'
        bgpath_subtitle_pits = os.path.dirname(__file__) + '/img/' + theme + '/subtitle/bg_subtitle_pits.png'
        bgpath_blank = os.path.dirname(__file__) + '/img/' + theme + '/blank.png'
        textureId_ad = ac.newTexture(os.path.dirname(__file__) + '/img/' + theme + '/ad/bg_ad.png')

        # Position increase indicators
        bgpath_position_up = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_info_arrow_up_align_left.png'
        bgpath_position_down = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_info_arrow_down_align_left.png'
        bgpath_position_equal = os.path.dirname(__file__) + '/img/' + theme + '/standing/bg_standing_info_equal_sign_align_left.png'

        # Init some variables
        appStartTime = time.clock()
        maxDriverCount = ac.getCarsCount()
        trackLength = ac.getTrackLength()

        ac.initFont(0, fontname, 0, 0)

        # Initialize cars
        initCars()

        # Create IU objects
        SimuRacerCast["standing"] = StandingWindow("SimuRacerCast Tiempos", "Tiempos", driverDisplayedGlobal)
        SimuRacerCast["session_info"] = SessionInfoWindow("SimuRacerCast Info Sesion", "Session")
        SimuRacerCast["subtitle"] = SubtitleWindow("SimuRacerCast Subtitulo", "Subtitle")
        SimuRacerCast["controller"] = ControllerWindow("SimuRacerCast Controlador", "Controller")
        SimuRacerCast["config"] = ConfigWindow("SimuRacerCast Config", "Config")
        SimuRacerCast["ad"] = AdWindow("SimuRacerCast Publi", "Ad")
        SimuRacerCast["fastest_lap"] = FastestLapWindow("SimuRacerCast Fastest Lap", "Fastest Lap")
        SimuRacerCast["score_table"] = ScoreTableWindow("SimuRacerCast Puntos", "Puntos")

        # Initialize renders callbacks
        ac.addRenderCallback(SimuRacerCast["standing"].window, onRenderCallbackStanding)
        ac.addRenderCallback(SimuRacerCast["session_info"].window, onRenderCallbackSessionInfo)
        ac.addRenderCallback(SimuRacerCast["subtitle"].window, onRenderCallbackSubtitle)
        ac.addRenderCallback(SimuRacerCast["controller"].window, onRenderCallbackController)
        ac.addRenderCallback(SimuRacerCast["config"].window, onRenderCallbackConfig)
        ac.addRenderCallback(SimuRacerCast["ad"].window, onRenderCallbackAd)
        ac.addRenderCallback(SimuRacerCast["score_table"].window, onRenderCallbackScoreTable)

        return "SimuRacerCast"
    except Exception as e:
        ac_log("Error in acMain: %s" % e)



##############################################################
## UPDATE
##############################################################
def acUpdate(deltaT):
    try:
        global lastUpdateTime, bestRaceLapTime, lastBestRaceLapTime, lastTimeFastestLapUpdate
        global session_prev
        global bestRaceSector1, bestRaceSector2, bestRaceSector3, bestRaceSector1Car, bestRaceSector2Car, bestRaceSector3Car

        lastUpdateTime += deltaT

        # check if session changed, reset variables
        if session_prev != info.graphics.session:
            session_prev = info.graphics.session
            bestRaceSector1 = 99999999
            bestRaceSector2 = 99999999
            bestRaceSector3 = 99999999
            bestRaceSector1Car = 0
            bestRaceSector2Car = 0
            bestRaceSector3Car = 0
            bestRaceLapTime = 99999999
            initCars()

        initNewCars()

        UpdateCarInfo()

        if lastUpdateTime < float(updateTime)/1000:
            return

        if currentStandingWindowType == 2:
            cars.sort(key=attrgetter('maxSpeed'), reverse=True)
        elif ((len([car for car in cars if car.currentRealtimeLeaderboardPos != 0]) == 0)
            or (info.graphics.session != 2 and not any(car.laps > 0 for car in cars))):
            cars.sort(key=attrgetter('distance'), reverse=True)
        else:
            cars.sort(key=attrgetter('currentRealtimeLeaderboardPos'), reverse=False)
        # If there is a new fastest lap, it is displayed on window
        # and the window is hidden after 5 seconds
        if lastBestRaceLapTime != bestRaceLapTime and not keepHidingFastestLap:
            SimuRacerCast["fastest_lap"].updateView(True)
            lastBestRaceLapTime = bestRaceLapTime
            lastTimeFastestLapUpdate = time.time()
        elif time.time() - lastTimeFastestLapUpdate > 5.0:
            SimuRacerCast["fastest_lap"].updateView(False)

        # Update the views
        #ac.ext_perfBegin("standing")
        if StandingWindowVisible:
            SimuRacerCast["standing"].updateView()
        #ac.ext_perfEnd("standing")
        #ac.ext_perfBegin("session_info")
        if SessionInfoWindowVisible:
            SimuRacerCast["session_info"].updateView()
        #ac.ext_perfEnd("session_info")
        #ac.ext_perfBegin("subtitle")
        if SubtitleWindowVisible:
            SimuRacerCast["subtitle"].updateView()
        #ac.ext_perfEnd("subtitle")
        #ac.ext_perfBegin("controller")
        SimuRacerCast["controller"].updateView(lastUpdateTime)
        #ac.ext_perfEnd("controller")
        #ac.ext_perfBegin("ad")
        if AdWindowVisible:
            SimuRacerCast["ad"].updateView()
        #ac.ext_perfEnd("ad")
        #ac.ext_perfBegin("score_table")
        if ScoreTableWindowVisible:
            SimuRacerCast["score_table"].updateView()
        #ac.ext_perfEnd("score_table")

        lastUpdateTime = 0

    except Exception as e:
        ac_log("Error in acUpdate: %s" % e)



##############################################################
## RENDERING RELATED
##############################################################
def onRenderCallbackStanding(deltaT):
    try:
        SimuRacerCast["standing"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackStanding: %s" % e)

def onRenderCallbackSessionInfo(deltaT):
    try:
        SimuRacerCast["session_info"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackSessionInfo: %s" % e)

def onRenderCallbackSubtitle(deltaT):
    try:
        SimuRacerCast["subtitle"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackSubtitle: %s" % e)

def onRenderCallbackController(deltaT):
    try:
        SimuRacerCast["controller"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackController: %s" % e)

def onRenderCallbackConfig(deltaT):
    try:
        SimuRacerCast["config"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackConfig: %s" % e)

def onRenderCallbackAd(deltaT):
    try:
        SimuRacerCast["ad"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackAd: %s" % e)

def onRenderCallbackFastestLap(deltaT):
    try:
        SimuRacerCast["fastest_lap"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallbackAd: %s" % e)

def onRenderCallbackScoreTable(deltaT):
    try:
        SimuRacerCast["score_table"].onRenderCallback(deltaT)
    except Exception as e:
        ac_log("Error in onRenderCallScoreTable: %s" % e)

##############################################################
## CAR CLASS
##############################################################
class SimuRacerCastCar(object):
    slotId = 0
    acDriverName = ""
    driverName = ""
    shortDriverName = ""
    catName = ""
    teamName = ""
    lastConnectedTime = 0
    carName = ""
    carBadge = 0
    driverPhoto = 0
    tyreNameShort = ""
    bestLap = 0
    lastLap_prev = 0
    lastLap = 0
    lapTime = 0
    speedMS = 0
    maxSpeed = 0
    distance = 0.001
    distanceToNextCar = 0.0
    currentRealtimeLeaderboardPos = 0
    position_change = 0
    currentWorldPosition = None
    splineposition = 0.0
    laps = 0
    isConnected = 1
    lastIsConnected = 1
    lastTimeConnected = 0

    #Tiempos de los mejores sectores
    bestSector1 = 99999999
    bestSector2 = 99999999
    bestSector3 = 99999999

    bestLapTime = 99999999

    bestSplits1 = 99999999
    bestSplits2 = 99999999
    bestSplits3 = 99999999
    bestSplits4 = 99999999
    bestSplits5 = 99999999
    bestSplits6 = 99999999
    bestSplits7 = 99999999
    bestSplits8 = 99999999
    bestSplits9 = 99999999
    bestSplits10 = 99999999
    currentSplits1 = 0
    currentSplits2 = 0
    currentSplits3 = 0
    currentSplits4 = 0
    currentSplits5 = 0
    currentSplits6 = 0
    currentSplits7 = 0
    currentSplits8 = 0
    currentSplits9 = 0
    currentSplits10 = 0

    #Tiempos de los sectores
    currentSector1 = 0
    currentSector2 = 0
    currentSector3 = 0

    isEndOfSector1 = 0
    isEndOfSector2 = 0
    isEndOfSector3 = 0
    isEndOfSector4 = 0
    isEndOfSector5 = 0
    isEndOfSector6 = 0
    isEndOfSector7 = 0
    isEndOfSector8 = 0
    isEndOfSector9 = 0
    isEndOfSector10 = 0

    # Ultimo tiempo de sesion en la ultima vuelta
    lastTime = 0

    PerformanceMeter = 0
    isLapInvalidated = 0
    endOfSectorDelay = 0

    now = 0
    compensateLastTime = 0

    # Si estuvo en pit en la vuelta para poner el tiempo correcto al final
    wasInPit = 0
    isOutLap = 0
    isInPitline = 0
    isInPit = 0
    pitEntryTime = 0
    pitExitTime = 0
    pits = 0

    # Car's last lap
    lapToFinish = 99999999

    # WEC Race - lap when we have to ask the driver's name to the server
    lapToCheckName = -1

    def __init__(self, carId):
        try:
            self.slotId = carId
            self.acDriverName = ac.getDriverName(carId)
            self.setDriverNameAndTeamNameAndCatName()
            self.carName = ac.getCarName(carId)
            self.tyreNameShort = ac.getCarTyreCompound(carId)
            if (info.graphics.session == 2):
                self.currentRealtimeLeaderboardPos = ac.getCarRealTimeLeaderboardPosition(carId) + 1
            else:
                self.currentRealtimeLeaderboardPos = ac.getCarLeaderboardPosition(carId)

            self.splineposition = ac.getCarState(carId, acsys.CS.NormalizedSplinePosition)
            self.laps = ac.getCarState(carId, acsys.CS.LapCount)
            self.bestLapTime = ac.getCarState(carId, acsys.CS.BestLap)
            self.lastLap_prev = ac.getCarState(carId, acsys.CS.LastLap)
            self.lastLap = ac.getCarState(carId, acsys.CS.LastLap)
            self.distance = ac.getCarState(carId, acsys.CS.LapCount) + self.splineposition
            self.speedMS = ac.getCarState(carId, acsys.CS.SpeedMS)
            self.maxSpeed = 0
            self.isInPitline = ac.isCarInPitline(carId)
            self.isInPit = ac.isCarInPit(carId)
            self.pitEntryTime = 0
            self.pitExitTime = 0
            self.PerformanceMeter = ac.getCarState(carId, acsys.CS.PerformanceMeter)
            self.lastIsConnected = 1
            self.isConnected = 1
            self.lastTimeConnected = time.clock()
            self.isEndOfSector1 = 0
            self.isEndOfSector2 = 0
            self.isEndOfSector3 = 0
            self.isEndOfSector4 = 0
            self.isEndOfSector5 = 0
            self.isEndOfSector6 = 0
            self.isEndOfSector7 = 0
            self.isEndOfSector8 = 0
            self.isEndOfSector9 = 0
            self.isEndOfSector10 = 0
            self.position_change = 0
            self.pits = 0
            self.bestSector1 = 99999999
            self.bestSector2 = 99999999
            self.bestSector3 = 99999999
            self.bestSplits1 = 99999999
            self.bestSplits2 = 99999999
            self.bestSplits3 = 99999999
            self.bestSplits4 = 99999999
            self.bestSplits5 = 99999999
            self.bestSplits6 = 99999999
            self.bestSplits7 = 99999999
            self.bestSplits8 = 99999999
            self.bestSplits9 = 99999999
            self.bestSplits10 = 99999999
            self.currentSplits1 = 0
            self.currentSplits2 = 0
            self.currentSplits3 = 0
            self.currentSplits4 = 0
            self.currentSplits5 = 0
            self.currentSplits6 = 0
            self.currentSplits7 = 0
            self.currentSplits8 = 0
            self.currentSplits9 = 0
            self.currentSplits10 = 0
            self.lapToFinish = 99999999
            self.lapToCheckName = -1

            # Session time on last lap
            self.lapTime = 0

            # race session - if you were in pit on the lap, the last clock value is not lastLap
            if info.graphics.session == 2:
                self.isOutLap = 0
            elif self.isInPitline == 1 or ac.getCarState(self.slotId, acsys.CS.SpeedKMH) < 20:
                self.isOutLap = 1

            # load driver photo
            self.setDriverPhoto()

            # load badge
            self.carBadge = ac.newTexture("content/cars/" + self.carName + "/ui/badge.png")

        except Exception as e:
            ac.log("SimuRacerCast:bcastCar.__init__ %s" % e)
            ac.log("SimuRacerCast:bcastCar.__init__ {}-{} on {}".format(self.slotId, self.driverName, self.carName))

    def check(self):
        global cars, isWECRace
        global bestRaceSector1, bestRaceSector2, bestRaceSector3, bestRaceSector1Car, bestRaceSector2Car, bestRaceSector3Car
        global bestRaceLapTime, bestRaceLapName, bestRaceLapSlotId
        global lastFinishLineTime

        try:
            self.lapTime = 0
            self.lastConnectedTime = time.clock()

            #first we'll check wether the slot has been re-assigned
            if not isWECRace and self.driverName != str(ac.getDriverName(self.slotId)).strip():
                self.laps = 0
                self.bestLapTime = 99999999
                self.bestSplits1 = 99999999
                self.bestSplits2 = 99999999
                self.bestSplits3 = 99999999
                self.bestSplits4 = 99999999
                self.bestSplits5 = 99999999
                self.bestSplits6 = 99999999
                self.bestSplits7 = 99999999
                self.bestSplits8 = 99999999
                self.bestSplits9 = 99999999
                self.bestSplits10 = 99999999
                self.currentSplits1 = 0
                self.currentSplits2 = 0
                self.currentSplits3 = 0
                self.currentSplits4 = 0
                self.currentSplits5 = 0
                self.currentSplits6 = 0
                self.currentSplits7 = 0
                self.currentSplits8 = 0
                self.currentSplits9 = 0
                self.currentSplits10 = 0
                # Se inicializa en el cambio de piloto
                self.currentSector1 = 0
                self.currentSector2 = 0
                self.currentSector3 = 0
                self.bestSector1 = 99999999
                self.bestSector2 = 99999999
                self.bestSector3 = 99999999
                self.lastTime = 0
                self.pits = 0

            # Updating variables
            self.currentWorldPosition = ac.getCarState(self.slotId, acsys.CS.WorldPosition)
            self.splineposition = ac.getCarState(self.slotId, acsys.CS.NormalizedSplinePosition)
            self.lapTime = info.graphics.iCurrentTime - self.lastTime
            self.distance = ac.getCarState(self.slotId, acsys.CS.LapCount) + self.splineposition
            self.speedMS = ac.getCarState(self.slotId, acsys.CS.SpeedMS)
            # Checks the maximum speed of the pilot (checks if it is less than 400 due to incorrect values)
            speedKMH = ac.getCarState(self.slotId, acsys.CS.SpeedKMH)
            if self.maxSpeed < speedKMH and speedKMH < 400:
                self.maxSpeed = speedKMH
            self.lastIsConnected = self.isConnected
            self.isConnected = ac.isConnected(self.slotId)
            if self.isConnected:
                self.lastTimeConnected = time.clock()
            self.tyreNameShort = ac.getCarTyreCompound(self.slotId)
            self.laps = ac.getCarState(self.slotId, acsys.CS.LapCount)

            self.bestLapTime = ac.getCarState(self.slotId, acsys.CS.BestLap)

            self.lastLap_prev = self.lastLap
            self.lastLap = ac.getCarState(self.slotId, acsys.CS.LastLap)

            # Muestra el tiempo real de los sectores si no está invalidada la vuelta
            if self.splineposition >= 0.1 and self.splineposition < 0.4 and self.lastTime != 0:
                self.currentSector1 = self.lapTime
                self.currentSector2 = 0
                self.currentSector3 = 0
            if self.splineposition >= 0.4 and self.splineposition < 0.7 and self.lastTime != 0:
                self.currentSector2 = self.lapTime - self.currentSector1
                if self.currentSector1 > 0 and self.bestSector1 > self.currentSector1:
                    self.bestSector1 = self.currentSector1
                    if bestRaceSector1 > self.currentSector1:
                        bestRaceSector1 = self.currentSector1
                        bestRaceSector1Car = self.slotId
            if self.splineposition >= 0.7 and self.splineposition <= 1 and self.lastTime != 0:
                self.currentSector3 = self.lapTime - self.currentSector2 - self.currentSector1
                if self.currentSector2 > 0 and self.bestSector2 > self.currentSector2:
                    self.bestSector2 = self.currentSector2
                    if bestRaceSector2 > self.currentSector2:
                        bestRaceSector2 = self.currentSector2
                        bestRaceSector2Car = self.slotId
            if (self.lastLap != self.lastLap_prev):
                now = info.graphics.iCurrentTime
                currentRealLastTime = now - self.lastTime
                self.lastTime = now

                # Updates last time that someone crossed finish line
                lastFinishLineTime = time.clock()

                if debug:
                    ac.log("--------------")
                    ac.log("coche: %s" % "{}".format(self.driverName))
                    ac.log("now: %d" % now)
                    ac.log("now anterior: %d" % (self.lastTime))
                    ac.log("vuelta con nows: %d" % (now - self.lastTime))
                    ac.log("vuelta now-lasttime: %d" % currentRealLastTime)
                    ac.log("vuelta lastLap: %d" % (self.lastLap))
                    ac.log("diferencia now-lasttime y lastlap: %d" % (currentRealLastTime-self.lastLap))
                    ac.log("Compensacion: %d" % (self.lastLap - currentRealLastTime))
                    ac.log("Final: %d" % (abs(self.lastLap - currentRealLastTime + now - self.lastTime)))
                    ac.log("now siguiente sin compensar: %d" % now)
                    ac.log("Estuvo en Pit: %d" % self.wasInPit)

                if(self.lastTime != 0 and self.lastLap != 0 and self.isInPitline == 0 and self.wasInPit != self.laps):
                    self.currentSector3 = self.lastLap - self.currentSector2 - self.currentSector1

                    # Checks if the 3rd sector was the fastest one
                    if self.currentSector3 > 0 and self.bestSector3 > self.currentSector3:
                        self.bestSector3 = self.currentSector3
                        if bestRaceSector3 > self.currentSector3:
                            bestRaceSector3 = self.currentSector3
                            bestRaceSector3Car = self.slotId

                    # Checks if it was the fastest lap of the race
                    if self.laps > 1 and bestRaceLapTime > self.lastLap and info.graphics.session == 2:
                        bestRaceLapTime = self.lastLap
                        bestRaceLapName = self.driverName
                        bestRaceLapSlotId = self.slotId

                    if debug:
                        ac.log("Sector 1: %d" % self.currentSector1)
                        ac.log("Sector 2: %d" % self.currentSector2)
                        ac.log("Sector 3: %d" % self.currentSector3)
                        ac.log("Suma: %s" % timeToString(self.currentSector1 + self.currentSector2 + self.currentSector3))

            if info.graphics.session == 2:
                pos = ac.getCarRealTimeLeaderboardPosition(self.slotId) + 1
            else:
                pos = ac.getCarLeaderboardPosition(self.slotId)

            # Only updates driver's position if nobody crossed the finish line in the last second (solves a bug)
            canUpdatePosition = (time.clock() - lastFinishLineTime) > 1

            self._checkOvertake(pos, canUpdatePosition)

            # If the race finished, we do not update the driver's position (or if someone crossed the finish line)
            if (info.graphics.session == 2 and self.laps < self.lapToFinish and canUpdatePosition) or info.graphics.session != 2:
                self.currentRealtimeLeaderboardPos = pos

            if (self.lapToFinish == 99999999 and self.splineposition > 0.1 and ((info.graphics.session < 2 and checkTimeOut()) or (info.graphics.session == 2 and ((pos == 1 and (checkTimeOut() or self.laps + 1 == info.graphics.numberOfLaps)) or (pos > 1 and isRaceFinished()))))):
                # Sets lapToFinish when is not a race and time is over OR it is a race, you're the first and time is over (or the current lap is the last) OR you're not the leader and the leader already has won the race.
                self.lapToFinish = self.laps + 1

            self._checkPits()

            # If it is a WEC race, we check if we must refresh the driver name
            if isWECRace and self.lapToCheckName == self.laps:
                self.lapToCheckName = -1
                self._setDriverName()

            # Checks if the car is in outlap
            self.isLapInvalidated = ac.getCarState(self.slotId, acsys.CS.LapInvalidated)
            if self.isInPitline == 1 or self.wasInPit == self.laps or self.isLapInvalidated == 1:
                self.isOutLap = 1
            else:
                self.isOutLap = 0

            if self.endOfSectorDelay > 0:
                self.endOfSectorDelay = self.endOfSectorDelay - 1

        except Exception as e:
            ac.log("SimuRacerCast:bcastCar.check %s" % e)
            ac.log("error1: %s" % sys.exc_info()[0])
            ac.log("error2: %s" % sys.exc_info()[1])
            ac.log("error2: %s" % sys.exc_info()[2])
            ac.log("error2: %s" % sys.exc_info()[3])

    def setDriverPhoto(self):
        if isWECRace:
            self.driverPhoto = ac.newTexture(os.path.dirname(__file__) + '/img/driver/' + self.teamName + '.png')
        else:
            self.driverPhoto = ac.newTexture(os.path.dirname(__file__) + '/img/driver/' + self.driverName + '.png')

    def _checkOvertake(self, pos, canUpdatePosition):
        global position_change_duration

        # If we are in an unfinished race and one driver overtakes another without being in the pits
        #   and does not do when someone crossed the finish line (this is to solve a bug ...), it is marked as overtaking
        # Also, if we are not in a race, the driver was not in pit and he/she wins a position, we mark overtaking
        # In the first lap (and only in the first lap), we check if it is not overtaking any driver that it is in pits too (to prevent overtake commentators)
        if (self.currentRealtimeLeaderboardPos > pos
            and ((info.graphics.session == 2 and canUpdatePosition and (not self.isInPitline) and (self.laps < self.lapToFinish))
            or (info.graphics.session != 2))):
            if self.laps == 0 and self.splineposition < 0.5:
                for car in cars:
                    if car.currentRealtimeLeaderboardPos == self.currentRealtimeLeaderboardPos:
                        #ac.log("overtaking: %s %s %s %s %s %s" % (self.laps, pos, self.currentRealtimeLeaderboardPos, self.driverName, car.driverName, car.isInPitline))
                        if car.isInPitline == 0:
                            self.position_change = position_change_duration
                        break;
            else:
                self.position_change = position_change_duration
        elif info.graphics.session == 2 and self.currentRealtimeLeaderboardPos < pos:
            # If the pilot loses a position during the race, we set the overtaking indicator to 0
            self.position_change = 0

    def _checkPits(self):
        oldIsInPitline = self.isInPitline
        self.isInPitline = ac.isCarInPitline(self.slotId)
        if (oldIsInPitline == 1 and self.isInPitline == 0):
            self.wasInPit = self.laps
            if isWECRace:
                self.lapToCheckName = self.laps + 1

        # Plus 1 to pits and controls pit time
        oldIsInPit = self.isInPit
        self.isInPit = self.isInPitline and ac.getCarState(self.slotId, acsys.CS.SpeedKMH) < 20
        if oldIsInPit == 1 and self.isInPit == 0 and self.laps > 0 and self.pitExitTime == 0 and self.isConnected and self.lastIsConnected:
            self.pits += 1
            self.pitExitTime = time.clock()
        elif oldIsInPit == 0 and self.isInPit == 1 and self.laps > 0:
            self.pitEntryTime = time.clock()
        elif self.isInPitline == 0:
            self.pitEntryTime = 0
            self.pitExitTime = 0

    def distanceTo(self, car):
        #self comparison: bad
        if self.slotId == car.slotId:
            return 202

        #return the vector length between both world positions
        #but we don't want to watch pitted cars:
        if ac.isCarInPit(self.slotId) == 1:
            return 201

        #same for standing cars
        if self.speedMS < 1:
            return 200

        if trackLength > 500:
            distance = 2 * (car.laps + car.splineposition - self.laps - self.splineposition) * trackLength / (car.speedMS + self.speedMS)
        else:
            # distance by x-y distance
            distance = math.hypot(car.currentWorldPosition[0] - self.currentWorldPosition[0], car.currentWorldPosition[2] - self.currentWorldPosition[2])

        #closeups in the pitlane should be widely multiplied
        if ac.isCarInPitline(car.slotId) == 1:
            distance = distance * 50

        return distance

    def resetIsEndOfSector(self):
        if self.endOfSectorDelay <= 0:
            self.isEndOfSector1 = 0
            self.isEndOfSector2 = 0
            self.isEndOfSector3 = 0
            self.isEndOfSector4 = 0
            self.isEndOfSector5 = 0
            self.isEndOfSector6 = 0
            self.isEndOfSector7 = 0
            self.isEndOfSector8 = 0
            self.isEndOfSector9 = 0
            self.isEndOfSector10 = 0

    def _setDriverName(self):
        global isWECRace

        acDriverName = str(ac.getDriverName(self.slotId)).strip()

        if isWECRace:
            self._setDriverNameWECRace(acDriverName)
        else:
            self.driverName = acDriverName

    def _requestToGetDriverName(self, teamName):
        global WECEndpoint
        teamNameUrl = urllib.parse.quote(teamName)
        url = WECEndpoint + teamNameUrl
        r = requests.get(url)

        if r.status_code == 200:
            self.driverName = r.text
        else:
            self.driverName = teamName

    def _setDriverNameWECRace(self, acDriverName):
        # The given name for WEC race is: "category team_name"
        # We get the driver name making a call to the WECEndpoint
        nameParts = acDriverName.split(" ", 1)
        if len(nameParts) > 1:
            auxName = nameParts[1].strip()
        else:
            auxName = nameParts[0].strip()

        requestThread = threading.Thread(target=self._requestToGetDriverName, args=[auxName])
        requestThread.daemon = True
        requestThread.start()

    def _getCategory(self):
        global isWECRace

        if isWECRace:
            acDriverName = str(ac.getDriverName(self.slotId)).strip()
            acDriverNameParts = acDriverName.split(" ", 1)
            if len(acDriverNameParts) > 1:
                return acDriverNameParts[0].strip().lower()
        return ""

    def _getTeamName(self):
        global isWECRace

        if isWECRace:
            acDriverName = str(ac.getDriverName(self.slotId)).strip()
            acDriverNameParts = acDriverName.split(" ", 1)
            if len(acDriverNameParts) > 1:
                return acDriverNameParts[1].strip()
        return ""

    def setDriverNameAndTeamNameAndCatName(self):
        self._setDriverName()
        self.catName = self._getCategory()
        self.teamName = self._getTeamName()
        self.shortDriverName = self._shortenDriverName()

    # Returns the driver name shorted.
    #   If driver's name contains less than 2 words or the first word length is lower than 2, we return the input without modifications.
    #   If driver's name contains more than 2 words, we return the first word character followed by a point and the last name's word.
    #   If driver's name contains 2 words and the first one length is greater than 2 characters, we return the first word character follower by a point and the second name's word.
    def _shortenDriverName(self):
        driverNameSplit = self.driverName.split()
        if len(driverNameSplit) < 2 or len(driverNameSplit[0]) <= 2:
            return str(self.driverName).strip()
        elif len(driverNameSplit) > 2:
            return driverNameSplit[0][0].upper() + ". " + driverNameSplit[len(driverNameSplit) - 1]
        else:
            return driverNameSplit[0][0].upper() + ". " + driverNameSplit[1]

##############################################################
## STANDING CLASS
##############################################################
class StandingWindow:
    showTyreInfo = 0
    showPitsNum = True

    def __init__(self, name, headerName, driverDisplayed):
        global config

        try:
            self.headerName = headerName
            self.window = ac.newApp(name)
            self.positionLabel = []
            self.nameLabel = []
            self.tyreLabel = []
            self.timeLabel = []
            self.pitLabel = []
            self.pitNumLabel = []
            self.carIdDisplayed = []
            self.currentDriverDisplayed = 1

            self.alpha = 1.0
            self.showPitsNum = True
            try:
                self.showTyreInfo = config.getint("Options", "showTyreInfo")
            except:
                self.showTyreInfo = 0
                updateIni("Options", "showTyreInfo", str(self.showTyreInfo))

            self.titleLabel = ac.addLabel(self.window, "   Max. speed (km/h)")
            ac.setFontAlignment(self.titleLabel, 'left')
            ac.setCustomFont(self.titleLabel, fontname, 0, 0)
            ac.setBackgroundTexture(self.titleLabel, bgpath_score_table_title)

            for index in range(maxDriverCount):
                self.positionLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.positionLabel[index], 'left')
                ac.setCustomFont(self.positionLabel[index], fontname, 0, 0)

                self.nameLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.nameLabel[index], 'left')
                ac.setCustomFont(self.nameLabel[index], fontname, 0, 0)

                self.tyreLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.tyreLabel[index], 'center')
                ac.setCustomFont(self.tyreLabel[index], fontname, 0, 0)

                self.timeLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.timeLabel[index], 'right')
                ac.setCustomFont(self.timeLabel[index], fontname, 0, 0)

                self.pitLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.pitLabel[index], 'right')
                ac.setCustomFont(self.pitLabel[index], fontname, 0, 0)

                self.pitNumLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.pitNumLabel[index], 'left')
                ac.setCustomFont(self.pitNumLabel[index], fontname, 0, 0)

                self.carIdDisplayed.append(-1)

            self.refreshParameters(driverDisplayed)
        except Exception as e:
            ac_log("Error in StandingWindow constructor: %s" % e)

    def refreshParameters(self, driverDisplayed):
        global isWECRace

        self.driverDisplayed = driverDisplayed

        ac.setIconPosition(self.window, -10000, -10000)
        ac.setTitle(self.window, "")

        self.widthPos   = sizeStanding * 4
        if isWECRace:
            self.widthName  = sizeStanding * 22
        else:
            self.widthName  = sizeStanding * 13
        self.widthTyre = sizeStanding * 4
        self.widthTime  = sizeStanding * 9
        self.widthPit  = sizeStanding * 6
        self.width      = self.widthPos + self.widthName + self.widthTime + self.widthPit
        self.height          = int(sizeStanding * 3.6)
        self.spacing_height = int(sizeStanding * 0.1)
        self.spacing = sizeStanding
        self.spacing_focused_car = sizeStanding

        ac.setFontSize(self.titleLabel, sizeStanding * 2)
        ac.setPosition(self.titleLabel, self.spacing, - (self.height + self.spacing_height))
        ac.setSize(self.titleLabel, self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre + self.widthTime, self.height)
        ac.setFontAlignment(self.titleLabel, "left")

        for index in range(maxDriverCount):
            ac.setFontSize(self.positionLabel[index], sizeStanding * 2)
            ac.setPosition(self.positionLabel[index], self.spacing_focused_car + self.spacing, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.positionLabel[index], self.widthPos, self.height)
            ac.setFontAlignment(self.positionLabel[index], "center")

            ac.setFontSize(self.nameLabel[index], sizeStanding * 2)
            ac.setPosition(self.nameLabel[index], self.spacing_focused_car + self.spacing + self.widthPos, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.nameLabel[index], self.widthName, self.height)
            ac.setFontAlignment(self.nameLabel[index], "left")

            ac.setFontSize(self.tyreLabel[index], sizeStanding * 2)
            ac.setPosition(self.tyreLabel[index], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.tyreLabel[index], self.widthTyre, self.height)
            ac.setVisible(self.tyreLabel[index], self.showTyreInfo)
            ac.setFontAlignment(self.tyreLabel[index], "center")

            ac.setFontSize(self.timeLabel[index], sizeStanding * 2)
            ac.setPosition(self.timeLabel[index], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.timeLabel[index], self.widthTime, self.height)
            ac.setVisible(self.timeLabel[index], showStandingDelta)
            ac.setFontAlignment(self.timeLabel[index], "right")

            ac.setFontSize(self.pitLabel[index], sizeStanding * 2)
            ac.setPosition(self.pitLabel[index], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre + self.widthTime, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.pitLabel[index], self.widthPit, self.height)
            ac.setFontAlignment(self.pitLabel[index], "right")

            ac.setFontSize(self.pitNumLabel[index], sizeStanding * 1.7)
            ac.setPosition(self.pitNumLabel[index], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre + self.widthTime, (index + 1) * (self.height + self.spacing_height))
            ac.setSize(self.pitNumLabel[index], self.widthPit, self.height)
            ac.setFontAlignment(self.pitNumLabel[index], "left")

            if ac.setBackgroundTexture(self.positionLabel[index], bgpath_pos) < 0:
                ac_log("failed to load bg img")
            if ac.setBackgroundTexture(self.nameLabel[index], bgpath_name) < 0:
                ac_log("failed to load bg img")
            if ac.setBackgroundTexture(self.tyreLabel[index], bgpath_tyre) < 0:
                ac_log("failed to load bg img")
            if ac.setBackgroundTexture(self.timeLabel[index], bgpath_info) < 0:
                ac_log("failed to load bg img")

        # Adjust window size, opacity and border
        ac.setSize(self.window, self.width, self.height)
        ac.setBackgroundOpacity(self.window, float(opacityStanding)/100)
        ac.drawBorder(self.window, showStandingBorder)

    def updateView(self):
        try:
            lastIndex = len(cars) - 1
            self.currentDriverDisplayed = lastIndex
            currentFocusedCarID = ac.getFocusedCar()

            # change alpha
            if StandingWindowVisible:
                self.alpha = 1.0
                isVisible = 1
            else:
                self.alpha = 0
                isVisible = 0

            # Saves functions in local variables to improve performance
            ac_setPosition = ac.setPosition
            ac_setVisible = ac.setVisible
            ac_setText = ac.setText
            ac_setBackgroundTexture = ac.setBackgroundTexture
            ac_setFontColor = ac.setFontColor
            currentSession = info.graphics.session

            displaysMaxSpeed = (currentStandingWindowType == 2)
            ac_setVisible(self.titleLabel, isVisible * displaysMaxSpeed)

            for driverIndex in range(maxDriverCount):
                if driverIndex <= lastIndex and info.graphics.status != 1:
                    if cars[driverIndex].slotId == currentFocusedCarID:
                        self.spacing_focused_car = sizeStanding
                    else:
                        self.spacing_focused_car = 0

                    ac_setPosition(self.positionLabel[driverIndex], self.spacing_focused_car + self.spacing, driverIndex * (self.height+self.spacing_height))
                    ac_setPosition(self.nameLabel[driverIndex], (self.spacing_focused_car + self.spacing + self.widthPos), driverIndex * (self.height+self.spacing_height))
                    ac_setPosition(self.tyreLabel[driverIndex], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName, driverIndex * (self.height + self.spacing_height))
                    ac_setPosition(self.timeLabel[driverIndex], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre, driverIndex * (self.height + self.spacing_height))
                    ac_setPosition(self.pitLabel[driverIndex], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre + self.widthTime, driverIndex * (self.height + self.spacing_height))
                    ac_setPosition(self.pitNumLabel[driverIndex], self.spacing_focused_car + self.spacing + self.widthPos + self.widthName + self.showTyreInfo * self.widthTyre + self.widthTime + int(sizeStanding * 1.5), driverIndex*(self.height+self.spacing_height) + int(1 * sizeStanding))

                    ac_setVisible(self.positionLabel[driverIndex], isVisible)
                    ac_setVisible(self.nameLabel[driverIndex], isVisible)
                    ac_setVisible(self.tyreLabel[driverIndex], self.showTyreInfo * isVisible)
                    ac_setVisible(self.timeLabel[driverIndex], isVisible)
                    ac_setVisible(self.pitLabel[driverIndex], isVisible)
                    ac_setVisible(self.pitNumLabel[driverIndex], 0)

                    # If it is a WEC race, we show a different layout to difference categories
                    if isWECRace:
                        route = os.path.dirname(__file__) + '/img/' + theme + '/mod_wec/bg_standing_pos_' + cars[driverIndex].catName + '.png'
                        ac_setBackgroundTexture(self.positionLabel[driverIndex], route)
                    else:
                        ac_setBackgroundTexture(self.positionLabel[driverIndex], bgpath_pos)

                    # Refresh position label
                    ac_setText(self.positionLabel[driverIndex], "{}".format( driverIndex + 1 ))

                    # If it is a WEC Race, we show the full team name. Otherwise, we show the shortened
                    if isWECRace and cars[driverIndex].teamName != "":
                        textToShow = cars[driverIndex].teamName
                    else:
                        textToShow = cars[driverIndex].shortDriverName
                    ac_setText(self.nameLabel[driverIndex], " " + textToShow)
                    self.carIdDisplayed[driverIndex] = cars[driverIndex].slotId

                    ac_setText(self.tyreLabel[driverIndex], "{}".format(cars[driverIndex].tyreNameShort))

                    if (cars[driverIndex].laps >= cars[driverIndex].lapToFinish) or (currentSession < 2 and checkTimeOut() and cars[driverIndex].isInPitline):
                        ac_setBackgroundTexture(self.pitLabel[driverIndex], bgpath_checkered)
                    elif cars[driverIndex].isInPitline:
                        ac_setBackgroundTexture(self.pitLabel[driverIndex], bgpath_pit)
                    elif self.showPitsNum and currentSession == 2:
                        ac_setBackgroundTexture(self.pitLabel[driverIndex], bgpath_pits_n)
                        ac_setText(self.pitNumLabel[driverIndex], "Pit {}".format(cars[driverIndex].pits))
                        ac.setVisible(self.pitNumLabel[driverIndex], isVisible)
                    else:
                        ac_setBackgroundTexture(self.pitLabel[driverIndex], bgpath_nopit)

                    deltaTime = -1
                    # Refresh time/distance
                    if currentStandingWindowType == 2:
                        # Max. speeds display mode
                        ac_setText(self.timeLabel[driverIndex], "{:04.1f}".format(cars[driverIndex].maxSpeed))
                    elif currentSession == 2:
                        # If Racing
                        # If it is a race and it is disconnected, we show DNF. If it is a WECRace, we wait 5 minutes to show it
                        if cars[driverIndex].laps < cars[driverIndex].lapToFinish and not cars[driverIndex].isConnected and (not isWECRace or (time.clock() - cars[driverIndex].lastTimeConnected) > 300):
                            ac_setText(self.timeLabel[driverIndex], "DNF")
                        else:
                            if currentStandingWindowType == 1:
                                # If we are displaying the gap respect the leader, the previous driver will be the leader
                                previousDriverIndex = 0
                                if driverIndex <= 0:
                                    ac_setText(self.timeLabel[driverIndex], "Leader")
                            else:
                                previousDriverIndex = driverIndex - 1
                                if driverIndex <= 0:
                                    if cars[driverIndex].laps > 1:
                                        ac_setText(self.timeLabel[driverIndex],  "{: >4} laps".format(cars[driverIndex].laps))
                                    else:
                                        ac_setText(self.timeLabel[driverIndex],  "{: >4} lap".format(cars[driverIndex].laps))

                            if driverIndex <= 0:
                                pass
                            elif cars[driverIndex].carName == SC:
                                ac_setText(self.timeLabel[driverIndex], "SC")
                            elif cars[driverIndex].laps >= cars[driverIndex].lapToFinish:
                                ac_setText(self.timeLabel[driverIndex], "")
                            else:
                                if ((cars[driverIndex].speedMS + cars[previousDriverIndex].speedMS)*trackLength)!=0:
                                    # Avoid division by zero when cars are stationary
                                    deltaTime = 2*(cars[previousDriverIndex].distance - cars[driverIndex].distance)/(cars[driverIndex].speedMS + cars[previousDriverIndex].speedMS)*trackLength
                                deltaToLabel(self.timeLabel[driverIndex], cars[previousDriverIndex].distance - cars[driverIndex].distance, (cars[driverIndex].speedMS + cars[previousDriverIndex].speedMS)/2)

                    # Practice, qualif
                    elif cars[driverIndex].bestLapTime == 0:
                        ac_setText(self.timeLabel[driverIndex], "No Time")
                    elif currentStandingWindowType == 1 and driverIndex > 0 and cars[0].bestLapTime > 0:
                        gap = cars[driverIndex].bestLapTime - cars[0].bestLapTime
                        ac_setText(self.timeLabel[driverIndex], "{}".format(formatTimeGap(gap)))
                    else:
                        ac_setText(self.timeLabel[driverIndex], timeToString(cars[driverIndex].bestLapTime))

                    if cars[driverIndex].position_change <= 0 or currentStandingWindowType == 2:
                        # The position did not change or the maximum speed table is being displayed
                        ac_setBackgroundTexture(self.timeLabel[driverIndex], bgpath_info)
                        if trackLength > 0 and deltaTime >= 0 and deltaTime <= time_in_battle and not (cars[driverIndex].laps >= cars[driverIndex].lapToFinish):
                            ac_setFontColor(self.positionLabel[driverIndex], 1, 1, 0.5, self.alpha)
                            ac_setFontColor(self.nameLabel[driverIndex], 1, 1, 0.5, self.alpha)
                            ac_setFontColor(self.tyreLabel[driverIndex], 1, 1, 0.5, self.alpha)
                            ac_setFontColor(self.timeLabel[driverIndex], 1, 1, 0.5, self.alpha)
                        else:
                            cars[driverIndex].position_change = 0
                            ac_setFontColor(self.positionLabel[driverIndex], 1, 1, 1, self.alpha)
                            ac_setFontColor(self.nameLabel[driverIndex], 1, 1, 1, self.alpha)
                            ac_setFontColor(self.tyreLabel[driverIndex], 1, 1, 1, self.alpha)
                            ac_setFontColor(self.timeLabel[driverIndex], 1, 1, 1, self.alpha)
                    elif (time.time() - driverRemovedTime > 1) and (currentSession == 2 or (cars[driverIndex].wasInPit != cars[driverIndex].laps and cars[driverIndex].wasInPit + 1 != cars[driverIndex].laps)):
                        # Do not show green arrow if the driver does the first lap of the qualy session
                        ac_setBackgroundTexture(self.timeLabel[driverIndex], bgpath_info_ot)
                        ac_setText(self.timeLabel[driverIndex], "")
                        cars[driverIndex].position_change -= 1

                else:
                    ac_setVisible(self.positionLabel[driverIndex], 0)
                    ac_setVisible(self.nameLabel[driverIndex], 0)
                    ac_setVisible(self.tyreLabel[driverIndex], 0)
                    ac_setVisible(self.timeLabel[driverIndex], 0)
                    ac_setVisible(self.pitLabel[driverIndex], 0)
                    ac_setVisible(self.pitNumLabel[driverIndex], 0)
                    ac_setText(self.pitNumLabel[driverIndex], "")
                    ac_setText(self.timeLabel[driverIndex], "")
                    self.carIdDisplayed[driverIndex] = -1

            # Adjust window size, opacity and border
            ac.setSize(self.window, self.width, (sizeStanding * 2 + self.spacing)*self.currentDriverDisplayed + self.spacing)

        except Exception as e:
            ac_log("Error in StandingWindow.updateView: %s" % e)
            ac.log("standing[driverIndex].speedMS: %s" % cars[driverIndex].speedMS)
            ac.log("standing[driverIndex-1].speedMS: %s" % cars[driverIndex-1].speedMS)
            ac.log(" tracklength: %s" % trackLength)

    def changeShowPitsNum(self):
        self.showPitsNum = not self.showPitsNum

    def setShowTyreInfo(self, value):
        self.showTyreInfo = value
        updateIni("Options", "showTyreInfo", self.showTyreInfo)

    def getShowTyreInfo(self):
        return self.showTyreInfo

    def onRenderCallback(self, deltaT):
        try:
            ac.setBackgroundOpacity(self.window, float(opacityStanding)/100)
            ac.drawBorder(self.window, showStandingBorder)
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Standing::onRenderCallback() %s" % e)


##############################################################
## SESSION INFO CLASS
##############################################################
class SessionInfoWindow:

    def __init__(self, name, headerName):
        try:
            self.alpha = 1.0
            self.headerName = headerName
            self.window = ac.newApp(name)

            ac.setSize(self.window, 30*sizeSessionInfo, 8*sizeSessionInfo)
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window,0.9)
            ac.setTitle(self.window, self.headerName)
            ac.setBackgroundTexture(self.window, bgpath_session_info)


            self.infoLabel = ac.addLabel(self.window, "")
            ac.setSize(self.infoLabel, int(3.5*sizeSessionInfo), int(3.5*sizeSessionInfo))
            ac.setFontSize(self.infoLabel, int(3.5*sizeSessionInfo))
            ac.setPosition(self.infoLabel, int(1*sizeSessionInfo), int(3.3*sizeSessionInfo))
            ac.setFontColor(self.infoLabel, 1,1,1,1)
            ac.setFontAlignment(self.infoLabel, "left")
            ac.setCustomFont(self.infoLabel, fontname, 0, 1)

            self.timeLabel = ac.addLabel(self.window, "")
            ac.setSize(self.timeLabel, int(3*sizeSessionInfo), int(3*sizeSessionInfo))
            ac.setFontSize(self.timeLabel, int(3*sizeSessionInfo))
            ac.setPosition(self.timeLabel, int(9.0*sizeSessionInfo), int(3.7*sizeSessionInfo))
            ac.setFontColor(self.timeLabel, 1,1,1,1)
            ac.setFontAlignment(self.timeLabel, "center")
            ac.setCustomFont(self.timeLabel, fontname, 0, 1)

            if debug:
                ac.log("SimuRacerCast_session_info::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_session_info::__init__() %s" % e)

    def refreshParameters(self):
        try:
            ac.setSize(self.window, 30*sizeSessionInfo, 8*sizeSessionInfo)
            ac.setSize(self.infoLabel, int(3.5*sizeSessionInfo), int(3.5*sizeSessionInfo))
            ac.setFontSize(self.infoLabel, int(3.5*sizeSessionInfo))
            ac.setPosition(self.infoLabel, int(1*sizeSessionInfo), int(3.3*sizeSessionInfo))

            ac.setSize(self.timeLabel, int(3*sizeSessionInfo), int(3*sizeSessionInfo))
            ac.setFontSize(self.timeLabel, int(3*sizeSessionInfo))
            ac.setPosition(self.timeLabel, int(8.5*sizeSessionInfo), int(3.7*sizeSessionInfo))

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_session_info::refreshParameters() %s" % e)

    def updateView(self):
        try:
            if info.graphics.status == 1:
                ## replay
                ac.setText(self.timeLabel, "REPLAY")
            elif info.graphics.session == 0:
                ac.setText(self.infoLabel, "P")
                ac.setText(self.timeLabel, "{}".format(formatSessionInfoTime(info.graphics.sessionTimeLeft)))
            elif info.graphics.session == 1:
                ac.setText(self.infoLabel, "Q")
                ac.setText(self.timeLabel, "{}".format(formatSessionInfoTime(info.graphics.sessionTimeLeft)))
            elif info.graphics.session == 2:
                ac.setText(self.infoLabel, "R")
                if cars[0].laps + 1 == cars[0].lapToFinish:
                    ac.setPosition(self.timeLabel, int(9.2*sizeSessionInfo), int(4*sizeSessionInfo))
                    ac.setFontSize(self.timeLabel, int(2.5*sizeSessionInfo))
                    ac.setText(self.timeLabel, "LAST LAP")
                elif cars[0].laps >= cars[0].lapToFinish:
                    ac.setPosition(self.timeLabel, int(9.2*sizeSessionInfo), int(4*sizeSessionInfo))
                    ac.setFontSize(self.timeLabel, int(2.5*sizeSessionInfo))
                    ac.setText(self.timeLabel, "FINISH")
                elif info.static.isTimedRace == 1:
                    ac.setText(self.timeLabel, "{}".format(formatSessionInfoTime(info.graphics.sessionTimeLeft)))
                else:
                    ac.setText(self.timeLabel, "{} / {}".format(cars[0].laps, info.graphics.numberOfLaps))

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast:err in session_info:updateView() %s" % e)


    def onRenderCallback(self, deltaT):
        ac.setBackgroundOpacity(self.window, 0)
        ac.setIconPosition(self.window, -7000, -3000)
        ac.setTitle(self.window, "")

        try:
            if self.alpha < 0:
                ac.setBackgroundTexture(self.window, bgpath_blank)
                ac.setVisible(self.infoLabel, 0)
                ac.setVisible(self.timeLabel, 0)
            else:
                ac.setBackgroundTexture(self.window, bgpath_session_info)
                ac.setVisible(self.infoLabel, 1)
                ac.setVisible(self.timeLabel, 1)

            # change alpha
            if SessionInfoWindowVisible == True:
                if self.alpha < 1.0:
                    self.alpha += 0.1
            else:
                if self.alpha > 0:
                    self.alpha -= self.alpha


        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_session_info::onRenderCallback() %s" % e)


##############################################################
## SUBTITLE CLASS
##############################################################
class SubtitleWindow:
    alpha = 1
    showPitsNum = True

    def __init__(self, name, headerName):
        global sizeSubtitle
        global bestRaceSector1, bestRaceSector2, bestRaceSector3, bestRaceSector1Car, bestRaceSector2Car, bestRaceSector3Car
        global bestRaceLapTime
        global xTest, yTest

        try:
            self.headerName = headerName
            self.window = ac.newApp(name)

            self.topspace = 11*sizeSubtitle
            ac.setSize(self.window, int(72.1*sizeSubtitle), int(14.7*sizeSubtitle)+self.topspace)
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window,0.9)
            ac.setTitle(self.window, self.headerName)
            ac.setBackgroundTexture(self.window, bgpath_subtitle)

            # labels for car ahead
            self.lblPosPrev = ac.addLabel(self.window, "")
            ac.setPosition(self.lblPosPrev, int(8)*sizeSubtitle, int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPosPrev, int(3.5*sizeSubtitle), int(3.5*sizeSubtitle))
            ac.setFontSize(self.lblPosPrev, int(2*sizeSubtitle))
            ac.setFontColor(self.lblPosPrev, 0.9,0.9,0.9,0.9)
            ac.setFontAlignment(self.lblPosPrev, "right")
            ac.setCustomFont(self.lblPosPrev, fontname, 0, 0)

            self.lblDriverNamePrev = ac.addLabel(self.window, "")
            ac.setPosition(self.lblDriverNamePrev, int(12.1*sizeSubtitle), int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblDriverNamePrev, int(24*sizeSubtitle), int(3.5*sizeSubtitle))
            ac.setFontSize(self.lblDriverNamePrev, int(2*sizeSubtitle))
            ac.setFontColor(self.lblDriverNamePrev, 0.9,0.9,0.9,0.9)
            ac.setFontAlignment(self.lblDriverNamePrev, "left")
            ac.setCustomFont(self.lblDriverNamePrev, fontname, 0, 0)

            self.lblLaptimePrev = ac.addLabel(self.window, "")
            ac.setPosition(self.lblLaptimePrev, int(35*sizeSubtitle), int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblLaptimePrev, int(12*sizeSubtitle), int(3.5*sizeSubtitle))
            ac.setFontSize(self.lblLaptimePrev, int(2*sizeSubtitle))
            ac.setFontColor(self.lblLaptimePrev, 0.9,0.9,0.9,0.9)
            ac.setFontAlignment(self.lblLaptimePrev, "right")
            ac.setCustomFont(self.lblLaptimePrev, fontname, 0, 0)

            # labels for currently focused car
            self.lblPos = ac.addLabel(self.window, "")
            ac.setPosition(self.lblPos, int(0.6*sizeSubtitle), int(4.9*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPos, 6*sizeSubtitle, 6*sizeSubtitle)
            ac.setFontSize(self.lblPos, 3*sizeSubtitle)
            ac.setFontAlignment(self.lblPos, "center")
            ac.setCustomFont(self.lblPos, fontname, 0, 1)

            self.lblDriverName = ac.addLabel(self.window, "")
            ac.setPosition(self.lblDriverName, int(7.7*sizeSubtitle), int(4.9*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblDriverName, 44*sizeSubtitle, 6*sizeSubtitle)
            ac.setFontSize(self.lblDriverName, 3*sizeSubtitle)
            ac.setFontAlignment(self.lblDriverName, "left")
            ac.setCustomFont(self.lblDriverName, fontname, 0, 1)

            self.lblGap = ac.addLabel(self.window, "")
            ac.setPosition(self.lblGap, int(50.9*sizeSubtitle), int(4.9*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblGap, int(12.5*sizeSubtitle), 6*sizeSubtitle)
            ac.setFontSize(self.lblGap, int(2.5*sizeSubtitle))
            ac.setFontColor(self.lblGap, 0.9,0.9,0.9,0.9)
            ac.setFontAlignment(self.lblGap, "center")
            ac.setCustomFont(self.lblGap, fontname, 0, 0)

            self.lblLaptime = ac.addLabel(self.window, "")
            ac.setPosition(self.lblLaptime, int(32.4*sizeSubtitle), int(9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblLaptime, int(19.7*sizeSubtitle), 4*sizeSubtitle)
            ac.setFontSize(self.lblLaptime, int(2.5*sizeSubtitle))
            ac.setFontAlignment(self.lblLaptime, "right")
            ac.setCustomFont(self.lblLaptime, fontname, 0, 0)

            # Tiempo Sector 1
            self.lblSector1 = ac.addLabel(self.window, "")
            ac.setPosition(self.lblSector1, int(6.5*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector1, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector1, int(1.7*sizeSubtitle))
            ac.setFontAlignment(self.lblSector1, "center")
            ac.setCustomFont(self.lblSector1, fontname, 0, 0)
            ac.setBackgroundTexture(self.lblSector1, bgpath_sector_normal)
            ac.setFontColor(self.lblSector1, 0.9, 0.9, 0.9, self.alpha*0.9)
            ac.setText(self.lblSector1,"-- S1 --")

            # Tiempo Sector 2
            self.lblSector2 = ac.addLabel(self.window, "")
            ac.setPosition(self.lblSector2, int(17*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector2, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector2, int(1.7*sizeSubtitle))
            ac.setFontAlignment(self.lblSector2, "center")
            ac.setCustomFont(self.lblSector2, fontname, 0, 0)
            ac.setBackgroundTexture(self.lblSector2, bgpath_sector_normal)
            ac.setFontColor(self.lblSector2, 0.9, 0.9, 0.9, self.alpha*0.9)
            ac.setText(self.lblSector2,"-- S2 --")

            # Tiempo Sector 3
            self.lblSector3 = ac.addLabel(self.window, "")
            ac.setPosition(self.lblSector3, int(27.5*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector3, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector3, int(1.7*sizeSubtitle))
            ac.setFontAlignment(self.lblSector3, "center")
            ac.setCustomFont(self.lblSector3, fontname, 0, 0)
            ac.setBackgroundTexture(self.lblSector3, bgpath_sector_normal)
            ac.setFontColor(self.lblSector3, 0.9, 0.9, 0.9, self.alpha*0.9)
            ac.setText(self.lblSector3,"-- S3 --")

            # Pits label subtitle bar
            self.lblPits = ac.addLabel(self.window, "")
            ac.setPosition(self.lblPits, int(62.9*sizeSubtitle), int(5.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPits, int(6*sizeSubtitle), 2.9*sizeSubtitle)
            ac.setFontSize(self.lblPits, int(2*sizeSubtitle))
            ac.setFontAlignment(self.lblPits, "center")
            ac.setCustomFont(self.lblPits, fontname, 0, 0)
            ac.setBackgroundTexture(self.lblPits, bgpath_subtitle_pits)
            ac.setFontColor(self.lblPits, 0.9, 0.9, 0.9, self.alpha*0.9)

            # Etiquetas para ver la posición x e y de la posición de otras
            self.lblXYTest = ac.addLabel(self.window, "")
            ac.setPosition(self.lblXYTest, sizeSubtitle, int((10.2*sizeSubtitle)+self.topspace))
            ac.setSize(self.lblXYtest, int(10*sizeSubtitle), 2*sizeSubtitle)
            ac.setFontSize(self.lblXYTest, int(2*sizeSubtitle))
            ac.setFontAlignment(self.lblXYTest, "center")
            ac.setCustomFont(self.lblXYTest, fontname, 0, 0)

            if debug:
                ac.log("SimuRacerCast_Subtitle::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Subtitle::__init__() %s" % e)

    def updateView(self):
        try:
            # Saves functions in local variables to improve performance
            ac_setVisible = ac.setVisible
            ac_setText = ac.setText
            ac_setFontColor = ac.setFontColor
            currentSession = info.graphics.session

            if self.alpha > 0.5:

                # update data of currently focused car
                currentFocusedCarID = ac.getFocusedCar()
                for car in cars:
                    if car.slotId == currentFocusedCarID:
                        currentFocusedCar = car

                # If it is a WEC Race, subtitle window colour changes depending on the category. Moreover, layout changes when it is displaying Last Lap
                if isWECRace:
                    if currentSubtitleWindowType == 0:
                        route = os.path.dirname(__file__) + '/img/' + theme + '/mod_wec/bg_subtitle_' + currentFocusedCar.catName + '.png'
                    else:
                        route = os.path.dirname(__file__) + '/img/' + theme + '/mod_wec/bg_subtitle_' + currentFocusedCar.catName + '_last_lap.png'
                    ac.setBackgroundTexture(self.window, route)
                elif currentSubtitleWindowType == 1:
                    ac.setBackgroundTexture(self.window, bgpath_subtitle_last_lap)
                else:
                    ac.setBackgroundTexture(self.window, bgpath_subtitle)

                ac_setVisible(self.lblPits, self.showPitsNum)
                ac_setVisible(self.lblSector1, 1)
                ac_setVisible(self.lblSector2, 1)
                ac_setVisible(self.lblSector3, 1)

                # Actualiza el tiempo de la vuelta (muestra el tiempo de pit en caso de encontrarse en el pitline)
                if currentSession == 2 and currentFocusedCar.isInPitline == 1 and currentFocusedCar.distance > 0.1:
                    if currentFocusedCar.laps < currentFocusedCar.lapToFinish and not currentFocusedCar.isConnected and (not isWECRace or (time.clock() - currentFocusedCar.lastTimeConnected) > 300):
                        ac_setText(self.lblLaptime, "DNF")
                    elif (currentFocusedCar.pitEntryTime != 0):
                        if currentFocusedCar.pitExitTime != 0:
                            pitTime = currentFocusedCar.pitExitTime - currentFocusedCar.pitEntryTime
                        else:
                            pitTime = time.clock() - currentFocusedCar.pitEntryTime
                        ac_setText(self.lblLaptime,"{:2.3f}".format(pitTime))
                    else:
                        ac_setText(self.lblLaptime,"-- PIT TIME --")
                elif currentSubtitleWindowType == 1:
                    ac_setText(self.lblLaptime,"{}".format(timeToString(ac.getCarState(currentFocusedCarID, acsys.CS.LastLap))))
                elif (currentFocusedCar.splineposition > 0.1 or currentFocusedCar.laps == 0) and currentFocusedCar.isInPitline == 0:
                    ac_setText(self.lblLaptime,"{}".format(timeToString(currentFocusedCar.lapTime)))
                elif currentFocusedCar.laps > 0:
                    ac_setText(self.lblLaptime,"{}".format(timeToString(ac.getCarState(currentFocusedCarID, acsys.CS.LastLap))))
                else:
                    ac_setText(self.lblLaptime,"")

                # We take the position of the car list when we are not in the race and no lap has been made yet
                #  otherwise we show the corresponding position
                if currentFocusedCar.currentRealtimeLeaderboardPos <= 0 or not any(car.laps > 0 for car in cars):
                    ac_setText(self.lblPos, "{}".format(cars.index(currentFocusedCar) + 1))
                else:
                    ac_setText(self.lblPos, "{}".format(currentFocusedCar.currentRealtimeLeaderboardPos))
                ac_setText(self.lblDriverName, "{}".format(currentFocusedCar.driverName))
                ac_setText(self.lblPits, "PITS {}".format(int(currentFocusedCar.pits)))

                # update data of the car ahead
                carAhead = None
                if currentFocusedCar.currentRealtimeLeaderboardPos <= 0 or (currentSession != 2 and not any(car.laps > 0 for car in cars)):
                    currentCarPos = cars.index(currentFocusedCar)
                    if currentCarPos != 0:
                        carAhead = cars[currentCarPos - 1]
                else:
                    for car in cars:
                        if car.currentRealtimeLeaderboardPos == currentFocusedCar.currentRealtimeLeaderboardPos - 1:
                            carAhead = car

                if carAhead != None:
                    ac_setText(self.lblDriverNamePrev, "{}".format(carAhead.driverName))
                    if currentSession == 2:
                        ac_setText(self.lblPosPrev, "{}".format(carAhead.currentRealtimeLeaderboardPos))
                        ac_setText(self.lblLaptimePrev, "{}".format(timeToString(carAhead.lastLap)))
                        timegap = currentFocusedCar.distanceTo(carAhead)
                        if timegap > 100:
                            ac_setText(self.lblGap, "")
                        elif timegap > 0:
                            ac_setText(self.lblGap, "{:+.3f}".format(timegap))
                    else:
                        if not any(car.laps > 0 for car in cars):
                            ac_setText(self.lblPosPrev, "{}".format(cars.index(carAhead) + 1))
                        else:
                            ac_setText(self.lblPosPrev, "{}".format(carAhead.currentRealtimeLeaderboardPos))
                        ac_setText(self.lblLaptimePrev, "{}".format(timeToString(carAhead.bestLapTime)))
                        gap = currentFocusedCar.bestLapTime - carAhead.bestLapTime
                        if currentFocusedCar.isInPitline == 1:
                            ac_setText(self.lblGap, "PIT")
                        elif currentFocusedCar.wasInPit == currentFocusedCar.laps and currentSession < 2:
                            ac_setText(self.lblGap, "Out Lap")
                        elif currentFocusedCar.isLapInvalidated == 1:
                            ac_setText(self.lblGap, "Invalid")
                        elif gap > 0:
                            ac_setText(self.lblGap, "{}".format(formatTimeGap(gap)))
                        else:
                            ac_setText(self.lblGap, "")
                else:
                    ac_setText(self.lblPosPrev, "")
                    ac_setText(self.lblDriverNamePrev, "")
                    ac_setText(self.lblLaptimePrev, "")
                    if currentFocusedCar.isInPitline == 1:
                        ac_setText(self.lblGap, "PIT")
                    elif currentFocusedCar.wasInPit == currentFocusedCar.laps and currentSession < 2:
                        ac_setText(self.lblGap, "Out Lap")
                    elif currentFocusedCar.isLapInvalidated == 1:
                        ac_setText(self.lblGap, "Invalid")
                    else:
                        ac_setText(self.lblGap, "")

                # Etiquetas de sectores
                if currentFocusedCar.currentSector1 != 0:
                    ac_setText(self.lblSector1,"{}".format(timeToString(currentFocusedCar.currentSector1)))
                else:
                    ac_setText(self.lblSector1,"-- S1 --")

                if currentFocusedCar.currentSector2 != 0:
                    ac_setText(self.lblSector2,"{}".format(timeToString(currentFocusedCar.currentSector2)))
                else:
                    ac_setText(self.lblSector2,"-- S2 --")

                if currentFocusedCar.currentSector3 != 0:
                    ac_setText(self.lblSector3,"{}".format(timeToString(currentFocusedCar.currentSector3)))
                else:
                    ac_setText(self.lblSector3,"-- S3 --")

                if currentFocusedCar.isInPitline == 1 and currentFocusedCar.pitEntryTime == 0:
                    # Changes the lbl position when shows the label "-- PIT TIME --"
                    ac.setPosition(self.lblLaptime, int(36*sizeSubtitle), int(9.5*sizeSubtitle) + self.topspace)
                else:
                    ac.setPosition(self.lblLaptime, int(32.4*sizeSubtitle), int(9.5*sizeSubtitle) + self.topspace)

                # Color de vuelta
                if currentSession == 2 and currentFocusedCar.isInPitline == 1:
                    ac_setFontColor(self.lblLaptime, 1, 0.5, 0, self.alpha*0.9)
                elif (currentFocusedCar.splineposition <= 0.1 and currentFocusedCar.laps > 0) or currentSubtitleWindowType == 1:
                    if bestRaceLapTime == currentFocusedCar.lastLap:
                        ac_setFontColor(self.lblLaptime, 0.9, 0, 0.9, self.alpha*0.9)
                    elif currentFocusedCar.bestLapTime == currentFocusedCar.lastLap:
                        ac_setFontColor(self.lblLaptime, 0, 0.9, 0, self.alpha*0.9)
                    else:
                        ac_setFontColor(self.lblLaptime, 0.9, 0.9, 0.9, self.alpha*0.9)
                else:
                    ac_setFontColor(self.lblLaptime, 0.9, 0.9, 0.9, self.alpha*0.9)

                ac_setFontColor(self.lblPosPrev, 0.9, 0.9, 0.9, self.alpha*0.9)
                ac_setFontColor(self.lblDriverNamePrev, 0.9, 0.9, 0.9, self.alpha*0.9)
                ac_setFontColor(self.lblLaptimePrev, 0.9, 0.9, 0.9, self.alpha*0.9)
                ac_setFontColor(self.lblPos, 0.9, 0.9, 0.9, self.alpha*0.9)
                ac_setFontColor(self.lblDriverName, 0.9, 0.9, 0.9, self.alpha*0.9)
                ac_setFontColor(self.lblGap, 0.9, 0.9, 0.9, self.alpha*0.9)

                # Sector 1 size
                if (currentFocusedCar.splineposition>0.1 and currentFocusedCar.splineposition<0.4) and currentFocusedCar.laps > 0:
                    ac.setSize(self.lblSector1, int(10*sizeSubtitle), 2.7*sizeSubtitle)
                    ac.setFontSize(self.lblSector1, int(2*sizeSubtitle))
                else:
                    ac.setSize(self.lblSector1, int(8*sizeSubtitle), 2.2*sizeSubtitle)
                    ac.setFontSize(self.lblSector1, int(1.7*sizeSubtitle))

                # Sector 2 size
                if (currentFocusedCar.splineposition>=0.4 and currentFocusedCar.splineposition<0.7) and currentFocusedCar.laps > 0:
                    ac.setSize(self.lblSector2, int(10*sizeSubtitle), 2.7*sizeSubtitle)
                    ac.setFontSize(self.lblSector2, int(2*sizeSubtitle))
                else:
                    ac.setSize(self.lblSector2, int(8*sizeSubtitle), 2.2*sizeSubtitle)
                    ac.setFontSize(self.lblSector2, int(1.7*sizeSubtitle))

                # Sector 3 size
                if (currentFocusedCar.splineposition>=0.7 and currentFocusedCar.splineposition<1.0) and currentFocusedCar.laps > 0:
                    ac.setSize(self.lblSector3, int(10*sizeSubtitle), 2.7*sizeSubtitle)
                    ac.setFontSize(self.lblSector3, int(2*sizeSubtitle))
                else:
                    ac.setSize(self.lblSector3, int(8*sizeSubtitle), 2.2*sizeSubtitle)
                    ac.setFontSize(self.lblSector3, int(1.7*sizeSubtitle))

                # Colores de Sectores
                # Sector 1
                ac.setBackgroundTexture(self.lblSector1, bgpath_sector_normal)
                if (currentFocusedCar.splineposition<=0.1 or currentFocusedCar.splineposition>=0.4) and currentFocusedCar.laps > 0:
                    if currentFocusedCar.currentSector1 == bestRaceSector1 and bestRaceSector1Car == currentFocusedCarID:
                        ac.setBackgroundTexture(self.lblSector1, bgpath_sector_sb)
                    elif currentFocusedCar.currentSector1 == currentFocusedCar.bestSector1:
                        ac.setBackgroundTexture(self.lblSector1, bgpath_sector_pb)
                # Sector 2
                ac.setBackgroundTexture(self.lblSector2, bgpath_sector_normal)
                if (currentFocusedCar.splineposition<=0.1 or currentFocusedCar.splineposition>=0.7) and currentFocusedCar.laps > 0:
                    if currentFocusedCar.currentSector2 == bestRaceSector2 and bestRaceSector2Car == currentFocusedCarID:
                        ac.setBackgroundTexture(self.lblSector2, bgpath_sector_sb)
                    elif currentFocusedCar.currentSector2 == currentFocusedCar.bestSector2:
                        ac.setBackgroundTexture(self.lblSector2, bgpath_sector_pb)
                # Sector 3
                ac.setBackgroundTexture(self.lblSector3, bgpath_sector_normal)
                if currentFocusedCar.splineposition<=0.1 and currentFocusedCar.laps > 1:
                    if currentFocusedCar.currentSector3 == bestRaceSector3 and bestRaceSector3Car == currentFocusedCarID:
                        ac.setBackgroundTexture(self.lblSector3, bgpath_sector_sb)
                    elif currentFocusedCar.currentSector3 == currentFocusedCar.bestSector3:
                        ac.setBackgroundTexture(self.lblSector3, bgpath_sector_pb)
            else:
                ac.setBackgroundTexture(self.window, bgpath_blank)
                ac_setVisible(self.lblPits, 0)
                ac_setVisible(self.lblSector1, 0)
                ac_setVisible(self.lblSector2, 0)
                ac_setVisible(self.lblSector3, 0)

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Subtitle:err in updateView() %s" % e)


    def onRenderCallback(self, deltaT):
        try:

            # Se actualiza la etiqueta lblXTest y lblYTest para ver el valor de xTest e yTest, usados para posicionar etiquetas
            #ac.setText(self.lblXYTest, "X:{0}  Y:{1}".format(float (xTest), float(yTest)))

            ac.setBackgroundOpacity(self.window, 0)
            ac.setIconPosition(self.window, -7000, -3000)
            ac.setTitle(self.window, "")

            # update data of currently focused car
            currentFocusedCarID = ac.getFocusedCar()
            for car in cars:
                if car.slotId == currentFocusedCarID:
                    currentFocusedCar = car

            if currentFocusedCar.driverPhoto >= 0:
                ac.glColor4f(1,1,1, self.alpha * (1 - hideDriverPhoto))
                ac.glQuadTextured(48*sizeSubtitle, 4, 15*sizeSubtitle, 15*sizeSubtitle, currentFocusedCar.driverPhoto)

            if currentFocusedCar.carBadge >= 0:
                ac.glColor4f(1, 1, 1, self.alpha)
                ac.glQuadTextured(int(46.9*sizeSubtitle), int(4.8*sizeSubtitle) + self.topspace, int(4.4*sizeSubtitle), int(4.4*sizeSubtitle), currentFocusedCar.carBadge)

            # change alpha
            if SubtitleWindowVisible == True:
                if self.alpha < 1.0:
                    self.alpha = self.alpha + 0.1
            else:
                if self.alpha > 0:
                    self.alpha = self.alpha - 0.1

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Subtitle::onRenderCallback() %s" % e)


    def refreshParameters(self):
        global sizeSubtitle
        try:
            self.topspace = 11*sizeSubtitle
            ac.setSize(self.window, int(72.1*sizeSubtitle), int(14.7*sizeSubtitle)+self.topspace)

            ac.setPosition(self.lblPosPrev, int(8*sizeSubtitle), int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPosPrev, int(3.5*sizeSubtitle), int(3.5*sizeSubtitle))
            ac.setFontSize(self.lblPosPrev, int(2*sizeSubtitle))

            ac.setPosition(self.lblDriverNamePrev, int(12.1*sizeSubtitle), int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblDriverNamePrev, int(24*sizeSubtitle), int(3.5*sizeSubtitle))
            ac.setFontSize(self.lblDriverNamePrev, int(2*sizeSubtitle))

            ac.setPosition(self.lblLaptimePrev, int(35*sizeSubtitle), int(1.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblLaptimePrev, int(12*sizeSubtitle), int(3*sizeSubtitle))
            ac.setFontSize(self.lblLaptimePrev, int(2*sizeSubtitle))

            ac.setPosition(self.lblPos, int(0.6*sizeSubtitle), int(4.9*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPos, 6*sizeSubtitle, 6*sizeSubtitle)
            ac.setFontSize(self.lblPos, 3*sizeSubtitle)

            ac.setPosition(self.lblDriverName, int(7.7*sizeSubtitle), int(4.9*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblDriverName, 44*sizeSubtitle, 6*sizeSubtitle)
            ac.setFontSize(self.lblDriverName, 3*sizeSubtitle)

            ac.setPosition(self.lblGap, int(50.9*sizeSubtitle), int(4.8*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblGap, int(12.5*sizeSubtitle), 6*sizeSubtitle)
            ac.setFontSize(self.lblGap, int(2.5*sizeSubtitle))

            ac.setPosition(self.lblLaptime, int(32.4*sizeSubtitle), int(9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblLaptime, int(19.7*sizeSubtitle), 4*sizeSubtitle)
            ac.setFontSize(self.lblLaptime, int(2.5*sizeSubtitle))

            ac.setPosition(self.lblPits, int(62.9*sizeSubtitle), int(5.4*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblPits, int(6*sizeSubtitle), 2.9*sizeSubtitle)
            ac.setFontSize(self.lblPits, int(2*sizeSubtitle))

            ac.setPosition(self.lblSector1, int(6.5*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector1, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector1, int(1.7*sizeSubtitle))

            ac.setPosition(self.lblSector2, int(17*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector2, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector2, int(1.7*sizeSubtitle))

            ac.setPosition(self.lblSector3, int(27.5*sizeSubtitle), (9.5*sizeSubtitle)+self.topspace)
            ac.setSize(self.lblSector3, int(8*sizeSubtitle), 2.2*sizeSubtitle)
            ac.setFontSize(self.lblSector3, int(1.7*sizeSubtitle))


        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Subtitle::refreshParameters %s" % e)

    def changeShowPitsNum(self):
        self.showPitsNum = not self.showPitsNum


##############################################################
## CONTROLLER CLASS
##############################################################
class ControllerWindow:

    def __init__(self, name, headerName):
        try:
            self.headerName = headerName
            self.window = ac.newApp(name)

            ac.setSize(self.window, 28*sizeController, int(2.5*sizeController))
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window,0.5)
            ac.setTitle(self.window, self.headerName)

            self.btnToggle = ac.addButton(self.window, "Next")
            ac.setPosition(self.btnToggle, int(2.5*sizeController), int(0.3*sizeController))
            ac.setSize(self.btnToggle, 4*sizeController, 2*sizeController)
            ac.setFontSize(self.btnToggle, int(1.2*sizeController))
            ac.addOnClickedListener(self.btnToggle, onClickControllerNext)

            self.lblNextCandidate = ac.addLabel(self.window, "")
            ac.setPosition(self.lblNextCandidate, 8*sizeController, int(0.3*sizeController))
            ac.setSize(self.lblNextCandidate, 6*sizeController, 2*sizeController)
            ac.setFontSize(self.lblNextCandidate, int(1.2*sizeController))

            self.textInput = ac.addTextInput(self.window,"TEXT_INPUT")
            ac.setPosition(self.textInput,1*sizeController,int(0.8*sizeController))
            ac.setSize(self.textInput,1*sizeController,1*sizeController)
            ac.setFontSize(self.textInput, int(0.6*sizeController))

            if debug:
                ac.log("SimuRacerCast_Controller::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Controller::__init__() %s" % e)

    def updateView(self, deltaT):
        global nextMostInterestingCar
        global lastCamFocusSwitchTime, minCamSwitchTime, lastCamTypeChangeTime, camFocusHold
        try:

            ac_getFocusedCar = ac.getFocusedCar

            if autoSwitch == False:
                if info.graphics.session == 2:
                    minDistance = 10003
                    for car in cars:
                        if car != None:
                            if car.slotId != ac_getFocusedCar():
                                for xcar in cars:
                                    if xcar != None:
                                        distance = car.distanceTo(xcar)
                                        if (distance > 0) and (distance <= time_in_battle) and (car.currentRealtimeLeaderboardPos < nextMostInterestingCar.currentRealtimeLeaderboardPos):
                                            nextMostInterestingCar = car
                                            minDistance = distance
                                        elif (distance > 0) and (minDistance > time_in_battle) and (distance < minDistance):
                                            nextMostInterestingCar = car
                                            minDistance = distance


                    # Update info of next candidate
                    if minDistance < time_in_battle:
                        ac.setFontColor(self.lblNextCandidate, 0.9,0.9,0,1)
                    else:
                        ac.setFontColor(self.lblNextCandidate, 1,1,1,1)

                    if nextMostInterestingCar != None:
                        ac.setText(self.lblNextCandidate, "P{:>2}  {} {:4.3f}".format(nextMostInterestingCar.currentRealtimeLeaderboardPos, nextMostInterestingCar.driverName, minDistance))

                else:
                    # find a car on Track with highest pos
                    nextMostInterestingCar = None
                    for car in cars:
                        if car != None:
                            if car.isOutLap == 0:
                                nextMostInterestingCar = car
                                break

                    # find a car on Track with highest pos including outlap
                    if nextMostInterestingCar == None:
                        for car in cars:
                            if car != None:
                                if car.isInPitline == 0 and car.isLapInvalidated == 0 and ac.getCarState(car.slotId, acsys.CS.SpeedKMH)>20:
                                    nextMostInterestingCar = car
                                    break

                    if nextMostInterestingCar != None:
                        ac.setText(self.lblNextCandidate, "P{:>2}  {}".format(nextMostInterestingCar.currentRealtimeLeaderboardPos, nextMostInterestingCar.driverName))

            elif autoSwitch == True:   # auto switch on
                lastCamFocusSwitchTime += deltaT
                lastCamTypeChangeTime += deltaT

                if info.graphics.session == 2:
                    minDistance = 10003
                    for car in cars:
                        if car != None:
                            for xcar in cars:
                                if xcar != None:
                                    distance = car.distanceTo(xcar)
                                    if (distance > 0) and (distance <= time_in_battle) and (car.currentRealtimeLeaderboardPos < nextMostInterestingCar.currentRealtimeLeaderboardPos):
                                        nextMostInterestingCar = car
                                        minDistance = distance
                                    elif (distance > 0) and (minDistance > time_in_battle) and (distance < minDistance):
                                        nextMostInterestingCar = car
                                        minDistance = distance




                    # Update info of next candidate
                    if minDistance < time_in_battle:
                        ac.setFontColor(self.lblNextCandidate, 0.9,0.9,0,1)
                    else:
                        ac.setFontColor(self.lblNextCandidate, 1,1,1,1)

                    if nextMostInterestingCar != None:
                        ac.setText(self.lblNextCandidate, "P{:>2}  {} {:4.3f} {:4.3f}".format(nextMostInterestingCar.currentRealtimeLeaderboardPos, nextMostInterestingCar.driverName, minDistance, lastCamFocusSwitchTime))
                        #if nextMostInterestingCar.slotId != ac.getFocusedCar():
                        if lastCamFocusSwitchTime > float(minCamSwitchTime): # when time went enough
                            changeCameraFocus(nextMostInterestingCar, 1)

                else:

                    if camFocusHold >0: # if cam is holded by a car, check if need to be unholded
                        if ac.isCarInPitline(camFocusHold)== 1:
                            camFocusHold = 0
                            if debug:
                                ac_log("Cam hold released because car is in pit")
                        #if ac.getCarState(camFocusHold, acsys.CS.LapInvalidated) == 1:
                            #camFocusHold = 0
                        if ac.getCarState(camFocusHold, acsys.CS.SpeedKMH) < 10:
                            camFocusHold = 0
                            if debug:
                                ac_log("Cam hold released because of speed limit")
                        for car in cars:
                            if car != None:
                                if car.slotId == camFocusHold:
                                    if (car.currentSplits1 != 0) and (car.bestSplits1 != 0) and (car.currentSplits1 > car.bestSplits1 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits2 != 0) and (car.bestSplits2 != 0) and (car.currentSplits2 > car.bestSplits2 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits3 != 0) and (car.bestSplits3 != 0) and (car.currentSplits3 > car.bestSplits3 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits4 != 0) and (car.bestSplits4 != 0) and (car.currentSplits4 > car.bestSplits4 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits5 != 0) and (car.bestSplits5 != 0) and (car.currentSplits5 > car.bestSplits5 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits6 != 0) and (car.bestSplits6 != 0) and (car.currentSplits6 > car.bestSplits6 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits7 != 0) and (car.bestSplits7 != 0) and (car.currentSplits7 > car.bestSplits7 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits8 != 0) and (car.bestSplits8 != 0) and (car.currentSplits8 > car.bestSplits8 + 300):
                                        camFocusHold = 0
                                    elif (car.currentSplits9 != 0) and (car.bestSplits9 != 0) and (car.currentSplits9 > car.bestSplits9 + 300):
                                        camFocusHold = 0

                                    elif car.isEndOfSector10 == 1 and car.endOfSectorDelay <= 0:
                                        camFocusHold = 0
                                        if debug:
                                            ac_log("Cam hold released because it's end of sector 3 record")

                    nextMostInterestingCar = None

                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector9 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits9) < (car.bestSplits9+100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector8 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits8) < (car.bestSplits8 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector7 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits7) < (car.bestSplits7 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector6 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits6) < (car.bestSplits6 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break



                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector5 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits5) < (car.bestSplits5 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector4 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits4) < (car.bestSplits4 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector3 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits3) < (car.bestSplits3 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break
                    # find a car just finished sector 2 with good record
                    if camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector2 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits2) < (car.bestSplits2 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector2 record".format(car.slotId))
                                        break



                    # find a car just finished sector 1 with good record
                    if nextMostInterestingCar == None and camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isEndOfSector1 == 1 and car.endOfSectorDelay <= 0:
                                    #car.resetIsEndOfSector()
                                    if (car.currentSplits1) < (car.bestSplits1 + 100):
                                        if car.isOutLap == 0:
                                            nextMostInterestingCar = car
                                            camFocusHold = nextMostInterestingCar.slotId
                                            if debug:
                                                ac_log("Cam locked at {} because of sector1 record".format(car.slotId))
                                        break



                    # find a car on Track with highest pos
                    if nextMostInterestingCar == None and camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isOutLap == 0:
                                    nextMostInterestingCar = car
                                    break

                    # find a car on Track with highest pos including outlap
                    if nextMostInterestingCar == None and camFocusHold == 0:
                        for car in cars:
                            if car != None:
                                if car.isInPitline == 0 and ac.getCarState(car.slotId, acsys.CS.SpeedKMH)>20:
                                    nextMostInterestingCar = car
                                    break

                    if nextMostInterestingCar != None:
                        ac.setText(self.lblNextCandidate, "P{:>2}  {} {:4.3f} {}".format(nextMostInterestingCar.currentRealtimeLeaderboardPos, nextMostInterestingCar.driverName, lastCamFocusSwitchTime, ac.getCarLeaderboardPosition(camFocusHold)))
                        #if nextMostInterestingCar.slotId != ac.getFocusedCar():
                        if lastCamFocusSwitchTime > float(minCamSwitchTime): # when time went enough
                            changeCameraFocus(nextMostInterestingCar, 1)

                    elif camFocusHold != 0 and camFocusHold != ac.getFocusedCar() and lastCamFocusSwitchTime > float(minCamSwitchTime):
                        for car in cars:
                            if car != None and car.slotId == camFocusHold:
                                nextMostInterestingCar = car
                        changeCameraFocus(nextMostInterestingCar, 1)

            for car in cars:
                if car != None:
                    car.resetIsEndOfSector()



        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Controller:err in updateView() %s" % e)


    def onRenderCallback(self, deltaT):
        ac.setBackgroundOpacity(self.window, 0.5)
        ac.setIconPosition(self.window, -7000, -3000)
        ac.setTitle(self.window, "")

        try:
            currentText = ac.getText(self.textInput)
            if currentText != None and currentText != "":
                hotKey(currentText)
                ac.setText(self.textInput, "")

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Controller::onRenderCallback() %s" % e)

    def refreshParameters(self):
        global sizeSubtitle
        try:
            ac.setSize(self.window, 28*sizeController, int(2.5*sizeController))
            ac.setTitle(self.window, self.headerName)

            ac.setPosition(self.btnToggle, int(2.5*sizeController), int(0.3*sizeController))
            ac.setSize(self.btnToggle, 4*sizeController, 2*sizeController)
            ac.setFontSize(self.btnToggle, int(1.2*sizeController))

            ac.setPosition(self.lblNextCandidate, 8*sizeController, int(0.3*sizeController))
            ac.setSize(self.lblNextCandidate, 6*sizeController, 2*sizeController)
            ac.setFontSize(self.lblNextCandidate, int(1.2*sizeController))

            ac.setPosition(self.textInput,1*sizeController,int(0.8*sizeController))
            ac.setSize(self.textInput,1*sizeController,1*sizeController)
            ac.setFontSize(self.textInput, int(0.6*sizeController))

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_Controller::refreshParameters %s" % e)

#########################################################
## SCORE_ENTITY CLASS
########################################################
class ScoreEntity:
    name = None
    oldScore = 0
    oldPos = None
    newScore = 0
    qPoints = 0
    drivers = []

    def __init__(self, oldPos, name, oldScore, drivers):
        self.oldPos = int(oldPos)
        self.name = str(name).strip()
        self.oldScore = int(oldScore)
        self.drivers = []

        # Create a ScoreEntity for each driver name received
        if drivers != None and len(drivers) > 1:
            for driver in drivers:
                self.drivers.append(ScoreEntity(0, driver, 0, None))

    def updateNewScore(self):
        global racePoints, qualyPoints, cars, racePoints, fastestLapPoints

        if len(self.drivers) > 0:
            # It is a team and we have to update each driver score
            self.newScore = self.oldScore
            for driver in self.drivers:
                driver.updateNewScore()
                self.newScore += driver.newScore
        else:
            currentDriverPos = None
            currentCar = None
            # Get driver position
            for index, car in enumerate(cars):
                if str(car.driverName).lower() == str(self.name).lower():
                    currentDriverPos = index
                    currentCar = car
                    break

            self.newScore = self.oldScore

            if (currentDriverPos == None):
                # Driver not found
                return

            if info.graphics.session == 1 and currentCar.laps > 1 and currentDriverPos < len(qualyPoints):
                self.qPoints = qualyPoints[currentDriverPos]
            elif info.graphics.session != 2:
                self.qPoints = 0

            # OldScore + qualyPoints
            self.newScore += self.qPoints

            if info.graphics.session == 2 and info.static.isTimedRace == 0 and currentDriverPos < len(racePoints):
                # Race (race points + fastest lap)
                if bestRaceLapName == self.name:
                    self.newScore += racePoints[currentDriverPos] + fastestLapPoints
                else:
                    self.newScore += racePoints[currentDriverPos]

##############################################################
## SCORE_TABLE CLASS
##############################################################
class ScoreTableWindow:
    maxSimracerCount = 0  # number of scoreTable simracers
    scoreTable = [] # Score Table
    status = "Disconnected" # Default value

    def __init__(self, name, headerName):
        try:
            self.headerName = headerName
            self.window = ac.newApp(name)
            ac.drawBorder(self.window, 0)
            self.titleLabel = None
            self.positionLabel = []
            self.nameLabel = []
            self.pointsLabel = []
            self.positionIncrementLabel = []

            self.titleLabel = ac.addLabel(self.window, "  Realtime championship")
            ac.setFontAlignment(self.titleLabel, 'left')
            ac.setCustomFont(self.titleLabel, fontname, 0, 0)

            self.emptyTable = ac.addLabel(self.window, "No data")
            ac.setFontAlignment(self.emptyTable, 'center')
            ac.setCustomFont(self.emptyTable, fontname, 0, 0)

            self.refreshParameters()

            serverPortFound = self._findServer(ac.getServerIP())
            if serverPortFound != None:
                self._connectToServer(serverPortFound)

        except Exception as e:
            ac_log("Error in ScoreTableWindow constructor: %s" % e)

    def _connectToServer(self, port):
        pass
        try:
            # If there is already data, we don't change anything
            if self.scoreTable != None and len(self.scoreTable) > 0:
                self.status = "Connected"
                return

            scoreTableServer = ac.getServerIP()
            if scoreTableServer != None and scoreTableServer != "" and port != None and port != "":
                url = "http://" + scoreTableServer + ":" + port + "/championship"
                teamsTable = []

                table = self._getUrlTableContent(url)

                if table != None and table != "":
                    gdp_table_data = table.tbody.find_all("tr")

                    if (table.thead.find_all("th")[2].contents[0] == "Team"):
                        teamsTable = self._getTeams(gdp_table_data)
                        url += "?team_score=1"
                        table = self._getUrlTableContent(url)
                        gdp_table_data = table.tbody.find_all("tr")

                    pos = 0
                    for row in gdp_table_data:
                        if len(teamsTable) > 0:
                            drivers = []
                            for driverTeamName in teamsTable:
                                driverTeamNameSplitted = driverTeamName.split("|")
                                if row.contents[3].contents[0] == driverTeamNameSplitted[1]:
                                    drivers.append(driverTeamNameSplitted[0])
                            newEntry = ScoreEntity(pos, row.contents[3].contents[0], row.contents[5].contents[0], drivers)
                        else:
                            # ScoreEntity(oldPos, name, oldScore, drivers)
                            newEntry = ScoreEntity(pos, row.contents[3].contents[0], row.contents[5].contents[0], None)
                        self.scoreTable.append(newEntry)
                        pos += 1

            #Find maximum number of simracers
            self.maxSimracerCount = len(self.scoreTable)
            if len(self.scoreTable) > 0:
                self.status = "Connected"
                self._saveServer(scoreTableServer, port)
            else:
                self.status = "Disconnected"

            for index in range(self.maxSimracerCount):
                self.positionLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.positionLabel[index], 'center')
                ac.setCustomFont(self.positionLabel[index], fontname, 0, 0)

                self.nameLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.nameLabel[index], 'left')
                ac.setCustomFont(self.nameLabel[index], fontname, 0, 0)

                self.pointsLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.pointsLabel[index], 'right')
                ac.setCustomFont(self.pointsLabel[index], fontname, 0, 0)

                self.positionIncrementLabel.append(ac.addLabel(self.window, ""))
                ac.setFontAlignment(self.positionIncrementLabel[index], 'right')
                ac.setCustomFont(self.positionIncrementLabel[index], fontname, 0, 0)

            self.refreshParameters()

        except Exception as e:
            ac_log("Error in ScoreTableWindow _connectToServer: %s" % e)

    def _saveServer(self, url, port):
        global scoreTableServers
        try:
            found = False
            for server in scoreTableServers:
                parts = server.split(":")
                urlFound = parts[0].split(",")[1]
                portFound = parts[1]
                if urlFound == url and portFound == port:
                    found = True
                    break
            if not found:
                timestamp = int(time.time())
                newServer = str(timestamp) + "," + url + ":" + port
                scoreTableServers.append(newServer)
                scoreTableServersString = "|".join(scoreTableServers)
                updateIni("Options", "scoreTableServers", scoreTableServersString)

        except Exception as e:
            if debug:
                ac.log("Error in ScoreTableWindow _saveServer: %s" % e)

    # Returns the server port and updates its timestamp. It also eliminates the rest of the servers that have not been connected in a year
    def _findServer(self, url):
        global scoreTableServers
        scoreTableServersTemp = []
        try:
            serverPortFound = None
            for server in scoreTableServers:
                timestampFound = server.split(",")[0]
                serverFound = server.split(",")[1]
                if serverFound.find(url) != -1:
                    serverPortFound = serverFound.split(":")[1]
                    serverToAppend = str(int(time.time())) + "," + serverFound
                else:
                    timestampNow = time.time()
                    difference = timestampNow - float(timestampFound)
                    if difference <= 31556952.0:
                        serverToAppend = server
                    else:
                        continue
                scoreTableServersTemp.append(serverToAppend)

            scoreTableServers = scoreTableServersTemp
            scoreTableServersString = "|".join(scoreTableServers)
            updateIni("Options", "scoreTableServers", scoreTableServersString)

            return serverPortFound
        except Exception as e:
            if debug:
                ac.log("Error in ScoreTableWindow _findServer: %s" % e)

    def connectToServer(self, port):
        self.status = "Connecting..."
        self._connectToServer(port)

    def _getUrlTableContent(self, url):
        try:
            r = requests.get(url, timeout = (1, 5))

            if r.status_code == 200 and r.text != None and r.text.find("Welcome to the stracker statistics query") == -1:
                text = r.text

                p1 = text.find("<table")
                p2 = text.find("</tbody>")

                splitedString = text[p1:p2+8]

                splitedString += "</table>"

                from bs4 import BeautifulSoup

                soup = BeautifulSoup(splitedString, features="html.parser")
                table = soup.find("table")
                return table
            else:
                return None

        except Exception as e:
            ac_log("Error in ScoreTableWindow constructor points request: %s" % e)

    def _getTeams(self, data):
        teams = []
        for row in data:
            # Add the driver and his/her team to the array as "driverName|driverTeam"
            stringToAppend = str(row.contents[3].contents[0]) + "|" + row.contents[5].contents[0]
            teams.append(stringToAppend)
        return teams

    def refreshParameters(self):
        global sizeScoreTable, opacityScoreTable

        ac.setIconPosition(self.window, -10000, -10000)
        ac.setTitle(self.window, "")

        self.widthPosition   = sizeScoreTable * 4
        self.widthTitle = sizeScoreTable * 22
        self.widthName  = sizeScoreTable * 20
        self.widthOldPoints = sizeScoreTable * 10
        self.widthSum  = sizeScoreTable * 6
        self.widthCurrentPoints  = sizeScoreTable * 10
        self.width      = self.widthPosition + self.widthName + self.widthOldPoints + self.widthSum
        self.height          = int(sizeScoreTable * 3.6)
        self.spacing_height = int(sizeScoreTable * 0.1)
        self.spacing = sizeScoreTable

        ac.setFontSize(self.titleLabel, sizeScoreTable * 2)
        ac.setPosition(self.titleLabel, self.spacing, 0.93 * (0 - (self.height + self.spacing_height)))
        ac.setSize(self.titleLabel, self.widthTitle, self.height)
        ac.setFontAlignment(self.titleLabel, "left")
        if ac.setBackgroundTexture(self.titleLabel, bgpath_score_table_title) < 0:
                ac_log("failed to load bg img")

        ac.setFontSize(self.emptyTable, sizeScoreTable * 2)
        ac.setPosition(self.emptyTable, self.spacing, 0)
        ac.setSize(self.emptyTable, self.widthTitle, self.height)
        ac.setFontAlignment(self.emptyTable, "center")
        if ac.setBackgroundTexture(self.emptyTable, bgpath_name) < 0:
                ac_log("failed to load bg img")

        # Adjust window size, opacity and border
        ac.setSize(self.window, self.width, self.height)
        ac.setBackgroundOpacity(self.window, float(opacityScoreTable) / 100)

        for index in range(self.maxSimracerCount):
            ac.setFontSize(self.positionLabel[index], sizeScoreTable * 2)
            ac.setPosition(self.positionLabel[index], self.spacing, index * (self.height + self.spacing_height))
            ac.setSize(self.positionLabel[index], self.widthPosition, self.height)
            ac.setFontAlignment(self.positionLabel[index], "center")

            ac.setFontSize(self.nameLabel[index], sizeScoreTable * 2)
            ac.setPosition(self.nameLabel[index], self.spacing + self.widthPosition, index * (self.height + self.spacing_height))
            ac.setSize(self.nameLabel[index], self.widthName, self.height)
            ac.setFontAlignment(self.nameLabel[index], "left")

            ac.setFontSize(self.pointsLabel[index], sizeScoreTable * 2)
            ac.setPosition(self.pointsLabel[index], self.spacing + self.widthPosition + self.widthName, index * (self.height + self.spacing_height))
            ac.setSize(self.pointsLabel[index], self.widthOldPoints, self.height)
            ac.setFontAlignment(self.pointsLabel[index], "right")

            ac.setFontSize(self.positionIncrementLabel[index], sizeScoreTable * 2)
            ac.setPosition(self.positionIncrementLabel[index], self.spacing + self.widthPosition + self.widthName + self.widthOldPoints, index * (self.height + self.spacing_height))
            ac.setSize(self.positionIncrementLabel[index], self.widthSum, self.height)
            ac.setFontAlignment(self.positionIncrementLabel[index], "right")

            if ac.setBackgroundTexture(self.positionLabel[index], bgpath_pos) < 0:
                ac_log("failed to load bg img")
            if ac.setBackgroundTexture(self.nameLabel[index], bgpath_name) < 0:
                ac_log("failed to load bg img")
            if ac.setBackgroundTexture(self.pointsLabel[index], bgpath_tyre) < 0:
                ac_log("failed to load bg img")

    def updateScores(self):
        try:
            if self.maxSimracerCount > 0:
                for scoreEntity in self.scoreTable:
                    scoreEntity.updateNewScore()

                # Sort score table by newScore
                self.scoreTable.sort(key=lambda scoreEntity: scoreEntity.newScore, reverse=True)

        except Exception as e:
            ac_log("Error in ScoreTableWindow.updateScores: %s" % e)

    def updateView(self):
        global bgpath_position_up, bgpath_position_down, bgpath_position_equal
        global ScoreTableWindowVisible
        try:
            if ScoreTableWindowVisible == 1:
                self.updateScores()

            if self.maxSimracerCount > 0:
                for labelIndex in range(self.maxSimracerCount):
                    ac.setPosition(self.positionLabel[labelIndex], self.spacing, labelIndex * (self.height + self.spacing_height))
                    ac.setPosition(self.nameLabel[labelIndex], self.spacing + self.widthPosition, labelIndex * (self.height + self.spacing_height))
                    ac.setPosition(self.pointsLabel[labelIndex], self.spacing + self.widthPosition + self.widthName, labelIndex * (self.height + self.spacing_height))
                    ac.setPosition(self.positionIncrementLabel[labelIndex], self.spacing + self.widthPosition + self.widthName + self.widthOldPoints, labelIndex * (self.height + self.spacing_height))

                    # Refresh text of the label ( name is purple if the driver has the fastest lap)
                    ac.setText(self.positionLabel[labelIndex], "{}".format(labelIndex + 1))
                    ac.setText(self.nameLabel[labelIndex], "{}".format(" " + self.scoreTable[labelIndex].name))
                    ac.setText(self.pointsLabel[labelIndex], "{}".format(self.scoreTable[labelIndex].oldScore))
                    ac.setText(self.positionIncrementLabel[labelIndex], "")

                    # Score
                    scoreDifference = self.scoreTable[labelIndex].newScore - self.scoreTable[labelIndex].oldScore
                    scoreLabel = str(self.scoreTable[labelIndex].newScore) + " (+" + str(scoreDifference) + ")"
                    ac.setText(self.pointsLabel[labelIndex], "{}".format(scoreLabel))

                    # Position
                    positionDiff = labelIndex - self.scoreTable[labelIndex].oldPos
                    ac.setText(self.positionIncrementLabel[labelIndex], "{}".format(str(abs(positionDiff)) + " "))
                    if positionDiff < 0:
                        if ac.setBackgroundTexture(self.positionIncrementLabel[labelIndex], bgpath_position_up) < 0:
                            ac_log("failed to load bg img")
                    elif positionDiff > 0:
                        if ac.setBackgroundTexture(self.positionIncrementLabel[labelIndex], bgpath_position_down) < 0:
                            ac_log("failed to load bg img")
                    else:
                        if ac.setBackgroundTexture(self.positionIncrementLabel[labelIndex], bgpath_position_equal) < 0:
                            ac_log("failed to load bg img")

        except Exception as e:
            ac_log("Error in ScoreTableWindow.updateView: %s" % e)

    def onRenderCallback(self, deltaT):
        global bestRaceLapName, ScoreTableWindowVisible, sizeScoreTable, opacityScoreTable

        ac.setBackgroundOpacity(self.window, float(opacityScoreTable) / 100)

        try:
            # change alpha
            if ScoreTableWindowVisible:
                self.alpha = 1
            else:
                self.alpha = 0

            ac.setVisible(self.titleLabel, self.alpha)
            ac.setVisible(self.emptyTable, self.alpha * (self.maxSimracerCount == 0))

            for index in range(self.maxSimracerCount):
                if self.scoreTable[index].name == bestRaceLapName:
                    ac.setFontColor(self.nameLabel[index], 0.4, 0.4, 0.9, self.alpha)
                else:
                    ac.setFontColor(self.nameLabel[index], 1, 1, 1, self.alpha)
                ac.setFontColor(self.positionLabel[index], 1, 1, 1, self.alpha)
                ac.setFontColor(self.pointsLabel[index], 1, 1, 1, self.alpha)
                ac.setFontColor(self.positionIncrementLabel[index], 1, 1, 1, self.alpha)

                ac.setVisible(self.positionLabel[index], self.alpha)
                ac.setVisible(self.nameLabel[index], self.alpha)
                ac.setVisible(self.pointsLabel[index], self.alpha)
                ac.setVisible(self.positionIncrementLabel[index], self.alpha)

            # Adjust window size, opacity and border
            ac.setSize(self.window, self.width, sizeScoreTable * 2 + self.spacing + self.spacing)

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_ScoreTableWindow::onRenderCallback() %s" % e)

    def getStatus(self):
        try:
            return self.status
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_ScoreTableWindow::onRenderCallback() %s" % e)

##############################################################
## CONFIGURATION CLASS
##############################################################
class ConfigWindow:

    def __init__(self, name, headerName):
        try:
            self.headerName = headerName
            self.window = ac.newApp(name)

            ac.setSize(self.window, 280, 680)
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window,0.5)
            ac.setTitle(self.window, self.headerName)

            self.chkAutoSwitch = ac.addCheckBox(self.window, "Auto Switch")
            ac.setPosition(self.chkAutoSwitch, 25, 40)
            ac.setSize(self.chkAutoSwitch, 40, 10)
            ac.setFontSize(self.chkAutoSwitch, 10)
            ac.addOnCheckBoxChanged(self.chkAutoSwitch, onClickCheckBoxAutoSwitch)
            ac.setValue(self.chkAutoSwitch, int(autoSwitch))

            self.lblSelectCamera = ac.addLabel(self.window, "< Select Cameras > {:2d}".format(len(selectedCameras)))
            ac.setPosition(self.lblSelectCamera, 25, 70)
            ac.setSize(self.lblSelectCamera, 60, 20)
            ac.setFontSize(self.lblSelectCamera, 20)

            camera_checkboxes = OrderedDict([("Cockpit", None),
                                             ("Car", None),
                                             ("Drivable", None),
                                             ("Track", None),
                                             ("Helicopter", None),
                                             ("OnBoardFree", None),
                                             ("Free", None),
                                             ("ImageGeneratorCamera", None),
                                             ("Start", None),
                                             ("Random", None),
                                             ])

            height_between = 20
            for i,camera_checkbox_label in enumerate(camera_checkboxes.keys()):
                camera_checkboxes[camera_checkbox_label] = ac.addCheckBox(self.window, camera_checkbox_label)
                ac.setPosition(camera_checkboxes[camera_checkbox_label], 25, 100 + i*height_between)
                ac.setSize(camera_checkboxes[camera_checkbox_label], 40, 10)
                ac.setFontSize(camera_checkboxes[camera_checkbox_label], 10)
                ac.addOnCheckBoxChanged(camera_checkboxes[camera_checkbox_label], onClickSelCam)
                if camera_checkbox_label in selectedCameras:
                    ac.setValue(camera_checkboxes[camera_checkbox_label], 1)

            self.lblSizeSessionInfo = ac.addLabel(self.window, "Session Info Size")
            ac.setPosition(self.lblSizeSessionInfo, 25, 320)
            ac.setSize(self.lblSizeSessionInfo, 60, 20)
            ac.setFontSize(self.lblSizeSessionInfo, 15)

            self.lblSizeSessionInfoValue = ac.addLabel(self.window, str(sizeSessionInfo))
            ac.setPosition(self.lblSizeSessionInfoValue, 200, 320)
            ac.setSize(self.lblSizeSessionInfoValue, 20, 20)
            ac.setFontSize(self.lblSizeSessionInfoValue, 15)
            ac.setFontAlignment(self.lblSizeSessionInfoValue, 'center')

            self.btnSizeSessionInfoPlus = ac.addButton(self.window, "+")
            ac.setPosition(self.btnSizeSessionInfoPlus, 230, 320)
            ac.setSize(self.btnSizeSessionInfoPlus, 20, 20)
            ac.setFontSize(self.btnSizeSessionInfoPlus, 12)
            ac.addOnClickedListener(self.btnSizeSessionInfoPlus, onClickSessionInfoSizePlus)

            self.btnSizeSessionInfoMinus = ac.addButton(self.window, "-")
            ac.setPosition(self.btnSizeSessionInfoMinus, 170, 320)
            ac.setSize(self.btnSizeSessionInfoMinus, 20, 20)
            ac.setFontSize(self.btnSizeSessionInfoMinus, 12)
            ac.addOnClickedListener(self.btnSizeSessionInfoMinus, onClickSessionInfoSizeMinus)


            self.lblSizeSubtitle = ac.addLabel(self.window, "Subtitle Size")
            ac.setPosition(self.lblSizeSubtitle, 25, 350)
            ac.setSize(self.lblSizeSubtitle, 60, 20)
            ac.setFontSize(self.lblSizeSubtitle, 15)

            self.lblSizeSubtitleValue = ac.addLabel(self.window, str(sizeSubtitle))
            ac.setPosition(self.lblSizeSubtitleValue, 200, 350)
            ac.setSize(self.lblSizeSubtitleValue, 20, 20)
            ac.setFontSize(self.lblSizeSubtitleValue, 15)
            ac.setFontAlignment(self.lblSizeSubtitleValue, 'center')

            self.btnSizeSubtitlePlus = ac.addButton(self.window, "+")
            ac.setPosition(self.btnSizeSubtitlePlus, 230, 350)
            ac.setSize(self.btnSizeSubtitlePlus, 20, 20)
            ac.setFontSize(self.btnSizeSubtitlePlus, 12)
            ac.addOnClickedListener(self.btnSizeSubtitlePlus, onClickSubtitleSizePlus)

            self.btnSizeSubtitleMinus = ac.addButton(self.window, "-")
            ac.setPosition(self.btnSizeSubtitleMinus, 170, 350)
            ac.setSize(self.btnSizeSubtitleMinus, 20, 20)
            ac.setFontSize(self.btnSizeSubtitleMinus, 12)
            ac.addOnClickedListener(self.btnSizeSubtitleMinus, onClickSubtitleSizeMinus)

            self.lblSizeController = ac.addLabel(self.window, "Controller Size")
            ac.setPosition(self.lblSizeController, 25, 380)
            ac.setSize(self.lblSizeController, 60, 20)
            ac.setFontSize(self.lblSizeController, 15)

            self.lblSizeControllerValue = ac.addLabel(self.window, str(sizeController))
            ac.setPosition(self.lblSizeControllerValue, 200, 380)
            ac.setSize(self.lblSizeControllerValue, 20, 20)
            ac.setFontSize(self.lblSizeControllerValue, 15)
            ac.setFontAlignment(self.lblSizeControllerValue, 'center')

            self.btnSizeControllerPlus = ac.addButton(self.window, "+")
            ac.setPosition(self.btnSizeControllerPlus, 230, 380)
            ac.setSize(self.btnSizeControllerPlus, 20, 20)
            ac.setFontSize(self.btnSizeControllerPlus, 12)
            ac.addOnClickedListener(self.btnSizeControllerPlus, onClickControllerSizePlus)

            self.btnSizeControllerMinus = ac.addButton(self.window, "-")
            ac.setPosition(self.btnSizeControllerMinus, 170, 380)
            ac.setSize(self.btnSizeControllerMinus, 20, 20)
            ac.setFontSize(self.btnSizeControllerMinus, 12)
            ac.addOnClickedListener(self.btnSizeControllerMinus, onClickControllerSizeMinus)


            self.lblSizeStanding = ac.addLabel(self.window, "Standing Size")
            ac.setPosition(self.lblSizeStanding, 25, 410)
            ac.setSize(self.lblSizeStanding, 60, 20)
            ac.setFontSize(self.lblSizeStanding, 15)

            self.lblSizeStandingValue = ac.addLabel(self.window, str(sizeStanding))
            ac.setPosition(self.lblSizeStandingValue, 200, 410)
            ac.setSize(self.lblSizeStandingValue, 20, 20)
            ac.setFontSize(self.lblSizeStandingValue, 15)
            ac.setFontAlignment(self.lblSizeStandingValue, 'center')

            self.btnSizeStandingPlus = ac.addButton(self.window, "+")
            ac.setPosition(self.btnSizeStandingPlus, 230, 410)
            ac.setSize(self.btnSizeStandingPlus, 20, 20)
            ac.setFontSize(self.btnSizeStandingPlus, 12)
            ac.addOnClickedListener(self.btnSizeStandingPlus, onClickStandingSizePlus)

            self.btnSizeStandingMinus = ac.addButton(self.window, "-")
            ac.setPosition(self.btnSizeStandingMinus, 170, 410)
            ac.setSize(self.btnSizeStandingMinus, 20, 20)
            ac.setFontSize(self.btnSizeStandingMinus, 12)
            ac.addOnClickedListener(self.btnSizeStandingMinus, onClickStandingSizeMinus)

            self.lblFastestLap = ac.addLabel(self.window, "Fastest Lap Size")
            ac.setPosition(self.lblFastestLap, 25, 440)
            ac.setSize(self.lblFastestLap, 60, 20)
            ac.setFontSize(self.lblFastestLap, 15)

            self.lblSizeFastestLapValue = ac.addLabel(self.window, str(sizeStanding))
            ac.setPosition(self.lblSizeFastestLapValue, 200, 440)
            ac.setSize(self.lblSizeFastestLapValue, 20, 20)
            ac.setFontSize(self.lblSizeFastestLapValue, 15)
            ac.setFontAlignment(self.lblSizeFastestLapValue, 'center')

            self.btnFastestLapPlus = ac.addButton(self.window, "+")
            ac.setPosition(self.btnFastestLapPlus, 230, 440)
            ac.setSize(self.btnFastestLapPlus, 20, 20)
            ac.setFontSize(self.btnFastestLapPlus, 12)
            ac.addOnClickedListener(self.btnFastestLapPlus, onClickFastestLapSizePlus)

            self.btnFastestLapMinus = ac.addButton(self.window, "-")
            ac.setPosition(self.btnFastestLapMinus, 170, 440)
            ac.setSize(self.btnFastestLapMinus, 20, 20)
            ac.setFontSize(self.btnFastestLapMinus, 12)
            ac.addOnClickedListener(self.btnFastestLapMinus, onClickFastestLapSizeMinus)

            self.chkShowTyreInfo = ac.addCheckBox(self.window, "Show Tyre Info")
            ac.setPosition(self.chkShowTyreInfo, 25, 470)
            ac.setSize(self.chkShowTyreInfo, 40, 10)
            ac.setFontSize(self.chkShowTyreInfo, 10)
            ac.addOnCheckBoxChanged(self.chkShowTyreInfo, onClickCheckBoxShowTyreInfo)
            ac.setValue(self.chkShowTyreInfo, SimuRacerCast["standing"].getShowTyreInfo())

            self.chkHideFastestLap = ac.addCheckBox(self.window, "Hide Fastest Lap")
            ac.setPosition(self.chkHideFastestLap, 25, 500)
            ac.setSize(self.chkHideFastestLap, 40, 10)
            ac.setFontSize(self.chkHideFastestLap, 10)
            ac.addOnCheckBoxChanged(self.chkHideFastestLap, onClickCheckBoxKeepHidingFastestLap)
            ac.setValue(self.chkHideFastestLap, keepHidingFastestLap)

            self.chkDriverPhoto = ac.addCheckBox(self.window, "Hide driver photo")
            ac.setPosition(self.chkDriverPhoto, 25, 530)
            ac.setSize(self.chkDriverPhoto, 40, 10)
            ac.setFontSize(self.chkDriverPhoto, 10)
            ac.addOnCheckBoxChanged(self.chkDriverPhoto, onClickCheckBoxHideDriverPhoto)
            ac.setValue(self.chkDriverPhoto, hideDriverPhoto)

            self.chkWECRace = ac.addCheckBox(self.window, "WEC Race")
            ac.setPosition(self.chkWECRace, 25, 560)
            ac.setSize(self.chkWECRace, 40, 10)
            ac.setFontSize(self.chkWECRace, 10)
            ac.addOnCheckBoxChanged(self.chkWECRace, onClickCheckBoxWECRace)
            ac.setValue(self.chkWECRace, isWECRace)

            self.lblPort = ac.addLabel(self.window, "--- Score server port ---")
            ac.setPosition(self.lblPort, 0, 590)
            ac.setSize(self.lblPort, 280, 20)
            ac.setFontSize(self.lblPort, 15)
            ac.setFontAlignment(self.lblPort, 'center')

            self.portInput = ac.addTextInput(self.window, "SERVER_PORT")
            ac.setPosition(self.portInput, 80, 620)
            ac.setSize(self.portInput, 120, 30)
            ac.setFontSize(self.portInput, 20)
            ac.addOnValidateListener(self.portInput, onPortChanged)

            self.lblConnectionStatus = ac.addLabel(self.window, "Disconnected")
            ac.setPosition(self.lblConnectionStatus, 0, 650)
            ac.setSize(self.lblConnectionStatus, 280, 20)
            ac.setFontSize(self.lblConnectionStatus, 15)
            ac.setFontAlignment(self.lblConnectionStatus, 'center')

            if debug:
                ac.log("SimuRacerCast_ConfigWindow::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_ConfigWindow::__init__() %s" % e)

    def updateView(self):
        try:
            pass

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_ConfigWindow:err in updateView() %s" % e)


    def onRenderCallback(self, deltaT):
        try:
            status = SimuRacerCast["score_table"].getStatus()
            ac.setText(self.lblConnectionStatus, status)
            if status == "Connected":
                ac.setFontColor(self.lblConnectionStatus, 0, 1, 0, 1)
            elif status == "Connecting...":
                ac.setFontColor(self.lblConnectionStatus, 0.9, 0.4, 0, 1)
            else:
                ac.setFontColor(self.lblConnectionStatus, 1, 0, 0, 1)

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_ConfigWindow::onRenderCallback() %s" % e)


##############################################################
## AD WINDOW CLASS
##############################################################
class AdWindow:
    global AdWindowVisible

    def __init__(self, name, headerName):
        try:
            self.headerName = headerName
            self.window = ac.newApp(name)

            ac.setSize(self.window, 10, 10)
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window,0.9)
            ac.setTitle(self.window, self.headerName)
            ac.setIconPosition(self.window, -1000, -1000)

            self.alpha = 0.0

            if debug:
                ac.log("SimuRacerCast_AdWindow::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_AdWindow::__init__() %s" % e)

    def updateView(self):
        try:
            pass

        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_AdWindow:err in updateView() %s" % e)


    def onRenderCallback(self, deltaT):
        #ac.setBackgroundOpacity(self.window, 0.9)

        ac.setTitle(self.window, "")

        try:
            if AdWindowVisible == True:
                ac.setBackgroundOpacity(self.window, 0.3)
                if textureId_ad != None:
                    ac.glColor4f(1,1,1,self.alpha)
                    ac.glQuadTextured(0, -80, 1920, 1080, textureId_ad)
                    if self.alpha < 1.0:
                        self.alpha = self.alpha + 0.01
            else:
                ac.setBackgroundOpacity(self.window, 0.0)
                if textureId_ad != None:
                    ac.glColor4f(1,1,1,self.alpha)
                    ac.glQuadTextured(0, -80, 1920, 1080, textureId_ad)
                    if self.alpha > 0:
                        self.alpha = self.alpha - 0.01


        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_AdWindow::onRenderCallback() %s" % e)


##############################################################
## FASTEST LAP WINDOW CLASS
##############################################################
class FastestLapWindow:

    def __init__(self, name, headerName):
        global bgpath_fastest_lap, sizeFastestLap
        try:
            self.headerName = headerName
            self.window = ac.newApp(name)
            ac.setVisible(self.window, 0)
            ac.setSize(self.window, 37.5*sizeFastestLap, 10*sizeFastestLap)
            ac.drawBorder(self.window,0)
            ac.setBackgroundOpacity(self.window, 0.0)
            ac.setIconPosition(self.window, -7000, -3000)
            ac.setTitle(self.window, "")
            ac.setBackgroundTexture(self.window, bgpath_fastest_lap)

            # Labels for driver and lap time
            self.lblDriverNameFL = ac.addLabel(self.window, "N/A")
            ac.setPosition(self.lblDriverNameFL, 5.5*sizeFastestLap, 5.7*sizeFastestLap)
            ac.setSize(self.lblDriverNameFL, 3.5*sizeFastestLap, 3.5*sizeFastestLap)
            ac.setFontSize(self.lblDriverNameFL, 2*sizeFastestLap)
            ac.setFontColor(self.lblDriverNameFL, 0.9, 0.9, 0.9, 0.9)
            ac.setFontAlignment(self.lblDriverNameFL, "left")
            ac.setCustomFont(self.lblDriverNameFL, fontname, 0, 1)

            self.lblFastestLapTime = ac.addLabel(self.window, "0:00:000")
            ac.setPosition(self.lblFastestLapTime, 28*sizeFastestLap, 5.7*sizeFastestLap)
            ac.setSize(self.lblFastestLapTime, 3.5*sizeFastestLap, 3.5*sizeFastestLap)
            ac.setFontSize(self.lblFastestLapTime, 2*sizeFastestLap)
            ac.setFontColor(self.lblFastestLapTime, 0.9, 0, 0.9, 0.9)
            ac.setFontAlignment(self.lblFastestLapTime, "center")
            ac.setCustomFont(self.lblFastestLapTime, fontname, 0, 1)

            self.showWindow()

            if debug:
                ac.log("SimuRacerCast_FastestLapWindow::__init__() finished")
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_FastestLapWindow::__init__() %s" % e)

    def showWindow(self):
        try:
            ac.setVisible(self.window, 0)
        except Exception as e:
            if debug:
                ac.log("SimuRacerCast_FastestLapWindow:err in showWindow() %s" % e)

    def refreshParameters(self):
        ac.setSize(self.window, 37.5*sizeFastestLap, 10*sizeFastestLap)

        # Labels for driver and lap time
        ac.setPosition(self.lblDriverNameFL, 5.5*sizeFastestLap, 5.7*sizeFastestLap)
        ac.setSize(self.lblDriverNameFL, 3.5*sizeFastestLap, 3.5*sizeFastestLap)
        ac.setFontSize(self.lblDriverNameFL, 2*sizeFastestLap)

        ac.setPosition(self.lblFastestLapTime, 28*sizeFastestLap, 5.7*sizeFastestLap)
        ac.setSize(self.lblFastestLapTime, 3.5*sizeFastestLap, 3.5*sizeFastestLap)
        ac.setFontSize(self.lblFastestLapTime, 2*sizeFastestLap)

    def updateView(self, wasFastestLap):
        global bestRaceLapTime, bestRaceLapName, bestRaceLapSlotId, keepHidingFastestLap
        if (keepHidingFastestLap == 0):
            try:
                if (wasFastestLap == False):
                    ac.setVisible(self.window, 0)
                else:
                    ac.setText(self.lblDriverNameFL, "{}".format(bestRaceLapName))
                    ac.setText(self.lblFastestLapTime, "{}".format(timeToString(ac.getCarState(bestRaceLapSlotId, acsys.CS.LastLap))))
                    ac.setVisible(self.window, 1)

            except Exception as e:
                if debug:
                    ac.log("SimuRacerCast_FastestLapWindow:err in updateView() %s" % e)


############################################
#   CALL BACK FUNCTIONS
############################################

def onClickControllerNext(*args):
    try:
        changeCameraFocus(nextMostInterestingCar, 1)
    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickControllerNext() %s" % e)


def onClickCheckBoxAutoSwitch(*args):
    global autoSwitch, lastCamFocusSwitchTime
    try:
        if args[1] == 1:
            autoSwitch = True
            lastCamFocusSwitchTime = 9
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxAutoSwitch() Auto Switch On")
        else:
            autoSwitch = False
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxAutoSwitch() Auto Switch Off")
    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickCheckBoxAutoSwitch() %s" % e)
    try:
        updateIni("Options", "autoSwitch", autoSwitch)
    except:
        ac_log("ERROR: Unable to update ini file")


def onClickCheckBoxShowTyreInfo(*args):
    try:
        SimuRacerCast["standing"].setShowTyreInfo(args[1])
        if debug:
            if args[1] == 1:
                ac.log("SimuRacerCast::onClickCheckBoxShowTyreInfo() Show Tyre Info On")
            else:
                ac.log("SimuRacerCast::onClickCheckBoxShowTyreInfo() Show Tyre Info Off")

    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickCheckBoxShowTyreInfo() %s" % e)


def onClickCheckBoxKeepHidingFastestLap(*args):
    global keepHidingFastestLap
    try:
        if args[1] == 1:
            keepHidingFastestLap = 1
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxKeepHidingFastestLap() Keep Hiding Fastest Lap On")
        else:
            keepHidingFastestLap = 0
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxKeepHidingFastestLap() Keep Hiding Fastest Lap Off")

        updateIni("Options", "keepHidingFastestLap", keepHidingFastestLap)
        SimuRacerCast["fastest_lap"].showWindow()


    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickCheckBoxKeepHidingFastestLap() %s" % e)

def onClickCheckBoxHideDriverPhoto(*args):
    global hideDriverPhoto
    try:
        if args[1] == 1:
            hideDriverPhoto = 1
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxHideDriverPhoto() Hide Driver Photo On")
        else:
            hideDriverPhoto = 0
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxHideDriverPhoto() Hide Driver Photo Off")

        updateIni("Options", "hideDriverPhoto", hideDriverPhoto)


    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickCheckBoxHideDriverPhoto() %s" % e)

def onClickCheckBoxWECRace(*args):
    global isWECRace, cars
    try:
        if args[1] == 1:
            isWECRace = 1
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxWECRace() isWECRace On")
        else:
            isWECRace = 0
            if debug:
                ac.log("SimuRacerCast::onClickCheckBoxWECRace() isWECRace Off")

        # Updates car's name, category and driver photo
        for car in cars:
            car.setDriverNameAndTeamNameAndCatName()
            car.setDriverPhoto()

        SimuRacerCast["standing"].refreshParameters(driverDisplayedGlobal)
        updateIni("Options", "isWECRace", isWECRace)


    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::onClickCheckBoxWECRace() %s" % e)

def onClickSelCam(*args):
    global selectedCameras
    if args[1] == 1: # selected
        selectedCameras.append(args[0])
    else: # not selected
        selectedCameras.remove(args[0])

    try:
        updateIni("Options", "selectedCameras", ",".join(selectedCameras))
    except:
        ac_log("ERROR: Unable to update ini file")

    ac.setText(SimuRacerCast["config"].lblSelectCamera, "< Select Cameras > {:2d}".format(len(selectedCameras)))


def onClickSessionInfoSizePlus(*args):
    global sizeSessionInfo
    sizeSessionInfo = sizeSessionInfo + 1
    ac.setText(SimuRacerCast["config"].lblSizeSessionInfoValue, "{}".format(sizeSessionInfo))
    SimuRacerCast["session_info"].refreshParameters()
    try:
        updateIni("Options", "sizeSessionInfo", str(sizeSessionInfo).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickSessionInfoSizeMinus(*args):
    global sizeSessionInfo
    if sizeSessionInfo > 3:
        sizeSessionInfo = sizeSessionInfo - 1
    ac.setText(SimuRacerCast["config"].lblSizeSessionInfoValue, "{}".format(sizeSessionInfo))
    SimuRacerCast["session_info"].refreshParameters()

    try:
        updateIni("Options", "sizeSessionInfo", str(sizeSessionInfo).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickSubtitleSizePlus(*args):
    global sizeSubtitle
    sizeSubtitle = sizeSubtitle + 1
    ac.setText(SimuRacerCast["config"].lblSizeSubtitleValue, "{}".format(sizeSubtitle))
    SimuRacerCast["subtitle"].refreshParameters()

    try:
        updateIni("Options", "sizeSubtitle", str(sizeSubtitle).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickSubtitleSizeMinus(*args):
    global sizeSubtitle
    if sizeSubtitle > 3:
        sizeSubtitle = sizeSubtitle - 1
    ac.setText(SimuRacerCast["config"].lblSizeSubtitleValue, "{}".format(sizeSubtitle))
    SimuRacerCast["subtitle"].refreshParameters()

    try:
        updateIni("Options", "sizeSubtitle", str(sizeSubtitle).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickControllerSizePlus(*args):
    global sizeController
    sizeController = sizeController + 1
    ac.setText(SimuRacerCast["config"].lblSizeControllerValue, "{}".format(sizeController))
    SimuRacerCast["controller"].refreshParameters()

    try:
        updateIni("Options", "sizeController", str(sizeController).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickControllerSizeMinus(*args):
    global sizeController
    if sizeController > 3:
        sizeController = sizeController - 1
    ac.setText(SimuRacerCast["config"].lblSizeControllerValue, "{}".format(sizeController))
    SimuRacerCast["controller"].refreshParameters()

    try:
        updateIni("Options", "sizeController", str(sizeController).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickStandingSizePlus(*args):
    global sizeStanding
    sizeStanding = sizeStanding + 1
    ac.setText(SimuRacerCast["config"].lblSizeStandingValue, "{}".format(sizeStanding))
    SimuRacerCast["standing"].refreshParameters(driverDisplayedGlobal)

    try:
        updateIni("Options", "sizeStanding", str(sizeStanding).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickStandingSizeMinus(*args):
    global sizeStanding
    if sizeStanding > 3:
        sizeStanding = sizeStanding - 1
    ac.setText(SimuRacerCast["config"].lblSizeStandingValue, "{}".format(sizeStanding))
    SimuRacerCast["standing"].refreshParameters(driverDisplayedGlobal)

    try:
        updateIni("Options", "sizeStanding", str(sizeStanding).lower())
    except:
        ac_log("ERROR: unable to update ini file")

def onClickFastestLapSizePlus(*args):
    global sizeFastestLap
    sizeFastestLap = sizeFastestLap + 1
    ac.setText(SimuRacerCast["config"].lblSizeFastestLapValue, "{}".format(sizeFastestLap))
    SimuRacerCast["fastest_lap"].refreshParameters()

    try:
        updateIni("Options", "sizeFastestLap", str(sizeFastestLap).lower())
    except:
        ac_log("ERROR: unable to update ini file")


def onClickFastestLapSizeMinus(*args):
    global sizeFastestLap
    if sizeFastestLap > 3:
        sizeFastestLap = sizeFastestLap - 1
    ac.setText(SimuRacerCast["config"].lblSizeFastestLapValue, "{}".format(sizeFastestLap))
    SimuRacerCast["fastest_lap"].refreshParameters()

    try:
        updateIni("Options", "sizeFastestLap", str(sizeFastestLap).lower())
    except:
        ac_log("ERROR: unable to update ini file")

def onPortChanged(*args):
    SimuRacerCast["score_table"].connectToServer(args[0])


def hotKey(keyin):
    global AdWindowVisible, SubtitleWindowVisible, SessionInfoWindowVisible, StandingWindowVisible, ScoreTableWindowVisible
    global currentStandingWindowType, StandingWindowTypes, currentSubtitleWindowType, SubtitleWindowTypes
    global xTest, yTest

    try:
        if keyin == "1":
            theCar = findCarIdFromPos(1)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "2":
            theCar = findCarIdFromPos(2)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "3":
            theCar = findCarIdFromPos(3)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "4":
            theCar = findCarIdFromPos(4)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "5":
            theCar = findCarIdFromPos(5)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "6":
            theCar = findCarIdFromPos(6)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "7":
            theCar = findCarIdFromPos(7)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "8":
            theCar = findCarIdFromPos(8)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "9":
            theCar = findCarIdFromPos(9)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "0":
            theCar = findCarIdFromPos(10)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "q":
            theCar = findCarIdFromPos(11)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "w":
            theCar = findCarIdFromPos(12)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "e":
            theCar = findCarIdFromPos(13)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "r":
            theCar = findCarIdFromPos(14)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "t":
            theCar = findCarIdFromPos(15)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "y":
            theCar = findCarIdFromPos(16)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "u":
            theCar = findCarIdFromPos(17)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "i":
            theCar = findCarIdFromPos(18)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "o":
            theCar = findCarIdFromPos(19)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "p":
            theCar = findCarIdFromPos(20)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "a":
            theCar = findCarIdFromPos(21)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "s":
            theCar = findCarIdFromPos(22)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "d":
            theCar = findCarIdFromPos(23)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "f":
            theCar = findCarIdFromPos(24)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "g":
            theCar = findCarIdFromPos(25)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "h":
            theCar = findCarIdFromPos(26)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "j":
            theCar = findCarIdFromPos(27)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "k":
            theCar = findCarIdFromPos(28)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "l":
            theCar = findCarIdFromPos(29)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "ñ":
            theCar = findCarIdFromPos(30)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "-":
            focusedCarId = ac.getFocusedCar()
            if info.graphics.session == 2: #race
                focusedCarPos = ac.getCarRealTimeLeaderboardPosition(focusedCarId)-1
            else:
                focusedCarPos = ac.getCarLeaderboardPosition(focusedCarId)-1
            theCar = findCarIdFromPos(focusedCarPos)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "+":
            focusedCarId = ac.getFocusedCar()
            if info.graphics.session == 2: #race
                focusedCarPos = ac.getCarRealTimeLeaderboardPosition(focusedCarId)+1
            else:
                focusedCarPos = ac.getCarLeaderboardPosition(focusedCarId)+1
            theCar = findCarIdFromPos(focusedCarPos)
            if theCar != None:
                changeCameraFocus(theCar, 0)
        elif keyin == "<":
            changeCameraFocus(nextMostInterestingCar, 0)
        elif keyin == "Z":
            StandingWindowVisible = not StandingWindowVisible
        elif keyin == "X":
            SubtitleWindowVisible = not SubtitleWindowVisible
        elif keyin == "C":
            SessionInfoWindowVisible = not SessionInfoWindowVisible
        elif keyin == "V":
            AdWindowVisible = not AdWindowVisible
        elif keyin == "P":
            ScoreTableWindowVisible = not ScoreTableWindowVisible
        elif keyin == "I":
            currentStandingWindowType = (currentStandingWindowType + 1) % len(StandingWindowTypes)
        elif keyin == "U":
            currentSubtitleWindowType = (currentSubtitleWindowType + 1) % len(SubtitleWindowTypes)
        elif keyin == "z":
            ac.setCameraMode(acsys.CM.Cockpit)
        elif keyin == "x":
            ac.setCameraMode(acsys.CM.Car)
        elif keyin == "c":
            ac.setCameraMode(acsys.CM.Drivable)
        elif keyin == "v":
            ac.setCameraMode(acsys.CM.Track)
        elif keyin == "b":
            ac.setCameraMode(acsys.CM.Helicopter)
        elif keyin == "n":
            ac.setCameraMode(acsys.CM.OnBoardFree)
        elif keyin == "m":
            ac.setCameraMode(acsys.CM.Free)
        elif keyin == ",":
            ac.setCameraMode(acsys.CM.Random)
        elif keyin == ".":
            ac.setCameraMode(acsys.CM.ImageGeneratorCamera)
        elif keyin == "-":
            ac.setCameraMode(acsys.CM.Start)
        elif keyin == "¡":
            SimuRacerCast["standing"].changeShowPitsNum()
            SimuRacerCast["subtitle"].changeShowPitsNum()
        elif keyin == "W":
            yTest=float(yTest-0.1)
        elif keyin == "S":
            yTest=float(yTest+0.1)
        elif keyin == "D":
            xTest=float(xTest+0.1)
        elif keyin == "A":
            xTest=float(xTest-0.1)
    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::hotkey() %s" % e)


def findCarIdFromPos(pos):
    theCar = None
    try:
        theCar = cars[pos - 1]
    except Exception as e:
        if debug:
            ac_log("findCarIdFromPos(): %s" % e)

    return theCar


def changeCameraFocus(car, type):   # car: car object to be focused,  type: 0 - no type change, 1 - type change
    global lastCamFocusSwitchTime, lastCamTypeChangeTime
    try:
        if type == 1 and lastCamTypeChangeTime > minCamTypeChangeTime and len(selectedCameras)>0:
            index = randint(0, len(selectedCameras)-1)
            lastCamTypeChangeTime = 0
            if selectedCameras[index] == "Cockpit":
                ac.setCameraMode(acsys.CM.Cockpit)
            elif selectedCameras[index] == "Car":
                ac.setCameraMode(acsys.CM.Car)
            elif selectedCameras[index] == "Drivable":
                ac.setCameraMode(acsys.CM.Drivable)
            elif selectedCameras[index] == "Track":
                ac.setCameraMode(acsys.CM.Track)
            elif selectedCameras[index] == "Helicopter":
                ac.setCameraMode(acsys.CM.Helicopter)
            elif selectedCameras[index] == "OnBoardFree":
                ac.setCameraMode(acsys.CM.OnBoardFree)
            elif selectedCameras[index] == "Free":
                ac.setCameraMode(acsys.CM.Free)
            elif selectedCameras[index] == "Random":
                ac.setCameraMode(acsys.CM.Random)
            elif selectedCameras[index] == "ImageGeneratorCamera":
                ac.setCameraMode(acsys.CM.ImageGeneratorCamera)
            elif selectedCameras[index] == "Start":
                ac.setCameraMode(acsys.CM.Start)
        if car != None and car.slotId != ac.getFocusedCar():
            ac.focusCar(car.slotId)
            lastCamFocusSwitchTime = 0

    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::changeCameraFocus() %s index %d" % (e, index))


#############################################################################
## COMMON UTIL FUNCTIONS
#############################################################################

def deltaToLabel(label, delta, speedAvg):
    if delta > 100:
        delta = 0

    distance = delta*trackLength

    if delta < 0:
        ac.setText(label, "{:+.1f}  s".format(0))
    elif delta < 0.5 and trackLength > 0:
        if unit == "time":
            if speedAvg < 15:
                speedAvg = 15
            deltaTime = distance/speedAvg
            ac.setText(label, "{:+.1f}  s".format(deltaTime))
        else:
            ac.setText(label, "")

    elif delta < 2 :
        ac.setText(label, "{:+.1f}  lap".format(delta))
    else:
        ac.setText(label, "{:+.1f}  laps".format(delta))


def timeToString(time):
    if time <= 0:
        return "-:--.---"
    else:
        return "{:d}:{:0>2d}.{:0>3d}".format(int(time/60000), int((time%60000)/1000), time%1000)


def formatTimeGap(time):
    laptime_sec = time//1000
    laptime_min = laptime_sec//60
    laptime_sec = laptime_sec%60
    laptime_milisec = time%1000

    if time <= 0:
        return "+{:>2}.{:03}".format(0)
    elif laptime_min == 0:
        return "+{:>2}.{:03}".format(laptime_sec,laptime_milisec)
    else:
        return "+{:>2}:{:02}.{:03}".format(laptime_min,laptime_sec,laptime_milisec)


def formatSessionInfoTime(time):
    s= time/1000
    m,s= divmod(s,60)
    h,m= divmod(m,60)

    if time <= 0:
        return "00:00"
    elif h > 0:
        return "{:02.0f}:{:02.0f}:{:02.0f}".format(h, m, s)
    else:
        return "{:02.0f}:{:02.0f}".format(m, s)

# This function updates info of cars. (one car at one call)
def UpdateCarInfo():
    global currentId, driverRemovedTime, appStartTime
    try:
        ## Refresh common info
        # update data of one car at a time
        now = time.clock()
        if currentId >= len(cars):
            currentId = 0
        if cars[currentId] != None:
            cars[currentId].check()
            if cars[currentId].lapTime < 30000 and cars[currentId].laps == 0 and cars[currentId].distance > 0.8 and cars[currentId].distance < 1.0:
                cars[currentId].distance -= 1.0

            if (info.graphics.session != 2 and now - appStartTime > 10 and not cars[currentId].isConnected) or cars[currentId].acDriverName != ac.getDriverName(cars[currentId].slotId):
                # If it is not a race, we remove driver when it is disconnected
                if debug:
                    ac.log("SimuRacerCast:UpdateCarInfo() - {} is disconnected. removing from the list".format(cars[currentId].driverName))
                driverRemovedTime = time.time()
                del cars[currentId]
        else:
            ac.log("CAR IS NONE %d" % currentId)

        currentId += 1
    except Exception as e:
        if debug:
            ac_log("err in UpdateCarInfo(): %s" % e)

# Creates a driver from the indicated index and adds it to the list of cars that is passed as a parameter
def _initCar(cars, i):
    # make car list
    newCarName = ac.getDriverName(i)
    if ac.isConnected(i) == 1 and newCarName != "" and not newCarName.lower() in skipDrivers:
        newcar = SimuRacerCastCar(i)
        cars.append(newcar)
        if debug:
            ac_log("added car {}, ID{}, {}".format(i, newcar.slotId, newcar.driverName))

def initNewCars():
    try:
        carCount = ac.getCarsCount()
        for i in range(carCount):
            existingCar = next((x for x in cars if x.slotId == i), None)
            if existingCar == None:
                _initCar(cars, i)

    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::InitNewCars  %s" % e)

def initCars():
    global cars
    try:
        carCount = ac.getCarsCount()
        del cars[:]
        for i in range(carCount):
            _initCar(cars, i)

    except Exception as e:
        if debug:
            ac.log("SimuRacerCast::InitCars  %s" % e)

    if debug:
        ac.log("SimuRacerCast::InitCars() done. total {} / {} cars".format(len(cars), carCount))

def checkTimeOut():
    if (((info.graphics.session == 2 and info.static.isTimedRace == 1) or (info.graphics.session < 2)) and info.graphics.sessionTimeLeft < 0):
        return True
    else:
        return False

def isRaceFinished():
    if (checkTimeOut() or (info.graphics.session == 2 and info.static.isTimedRace == 0)) and cars[0].laps == cars[0].lapToFinish:
        return True
    else:
        return False
