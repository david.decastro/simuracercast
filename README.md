# SimuRacerCast

SimuRacerCast es el resultado de un fork realizado sobre el BCast original, que se puede encontrar [aquí](https://www.assettocorsa.net/forum/index.php?threads/bcast-app-for-broadcasting-v1-12.32536/).

La principal finalidad de esta aplicación es servir como aplicación oficial para el canal de YouTube [SimuTV](https://www.youtube.com/channel/UCFTWcCj3nG1WMHbHIxVksCA), realizando las mejoras e implementando las nuevas funcionalidades que el equipo de comentaristas de este canal solicitan.

## Mejoras realizadas hasta la fecha

* Nombres de pilotos acortados en al barra lateral de tiempos.
* Pop-up cuando un piloto realiza una vuelta rápida en carrera.
* Visualización del número de paradas en boxes.
* Visualización del tiempo en boxes.
* Visualización de los tiempos por sector.
* Barra lateral de tiempos ordenada por velocidad máxima de los pilotos.
* Visualización del GAP con respeto al líder la carrera en la barra lateral de tiempos.
* Visualización de una flecha cuando un piloto adelanta a otro.
* Visualización en tiempo real de las puntuaciones de un campeonato.
* Mejoras de rendimiento / refactorización del código.
* Modo WEC Race: cambia el layout de visualización para distinguir categorías, además de hacer una petición a un servidor para obtener el nombre del piloto que compite para el equipo.

## Documentación - Manual_SimuRacerCast

En este repositorio se puede encontrar un documento PDF en el que se explica el funcionamiento de las funcionalidades más avanzadas.

## Actualizador automático - SRCastUpdater

Debido a que es bastante engorroso actualizar una aplicación de forma manual (descargando los datos de una URL y sobreescribiendo), hemos implementado un script en PS1 el cual se encarga de descargar la aplicación de nuestro repositorio y sobreescribir los datos. Además, tal y como se explica en el manual, es posible especificar si se quiere conservar el fichero de configuración para evitar perder toda la configuración realizada hasta el momento.

Para facilitar la ejecución de este PS1, hemos creado un ejecutable (.exe).

## Integrantes del equipo

El equipo está formado por:

* David Wee (david@damarada.net)
* David Noara (david.noara@gmail.com)
