﻿$url = "https://gitlab.com/david.decastro/simuracercast/-/archive/master/simuracercast-master.zip"
$outputZip = ".\simuracercast_temp.zip"
$outputFolder = ".\"
$acOutput = ".\assettocorsa\"

# Change to .exe location
$exeLocation = [Environment]::CurrentDirectory 
Push-Location $exeLocation

Write-Host "#################################################################"
Write-Host "#                         SimuRacerCast                         #"
Write-Host "#                Servicio de actualización 1.3.1                #"
Write-Host "#                       ©2021 David Noara                       #"
Write-Host "#################################################################"

$isPathCorrect = (Get-ChildItem -Path ".\" -Force -Directory "assettocorsa").Count -gt 0

if ($isPathCorrect -eq $false) {
    Write-Host "☠☠☠☠☠☠☠☠☠☠☠☠ERROR☠☠☠☠☠☠☠☠☠☠☠☠☠☠"
    Write-Host "☠                   ¡CARPETA INCORRECTA!                    ☠"
    Write-Host "☠     Este ejecutable debe encontrarse en el directorio     ☠"
    Write-Host "☠      previo al de Assetto Corsa (\steamapps\common\)      ☠"
    Write-Host "☠       Copie el ejecutable ahí y vuelva a intentarlo       ☠"
    Write-Host "☠                                                           ☠"
    Write-Host "☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠"

    Start-Sleep -s 5

    exit
}

$existsConfigFile = Test-Path "$acOutput\apps\python\SimuRacerCast\SimuRacerCast.ini"
$saveConfigFile = ""
if ($existsConfigFile -eq $true){
    while ($saveConfigFile -ne "s" -and $saveConfigFile -ne "n") {
        $saveConfigFile = Read-Host "¿Desea guardar el fichero de configuración que tiene actualmente? (s/n)"
    }
}

Write-Host "Comenzando descarga del servidor..."

$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $outputZip)

Write-Host "Extrayendo datos..."

Expand-Archive -LiteralPath $outputZip -DestinationPath $outputFolder -Force
Remove-Item -Path $outputZip

Write-Host "Copiando datos en la carpeta de Assetto Corsa..."

# Saves the SimuRacerCast config file if the user wants
if ($saveConfigFile -eq "s") {
    Copy-Item "$acOutput\apps\python\SimuRacerCast\SimuRacerCast.ini" -Destination $outputFolder -Force | Out-Null 
}

Copy-Item -Path "$outputFolder\simuracercast-master\assettocorsa\*" -Destination $acOutput -Recurse -Force | Out-Null

if ($saveConfigFile -eq "s") {
    Copy-item "$outputFolder\SimuRacerCast.ini" -Destination "$acOutput\apps\python\SimuRacerCast\" -Force | Out-Null
}

Write-Host "Limpiando ficheros temporales..."

Remove-Item -Path "$outputFolder\simuracercast-master\" -Recurse -Force

if ($saveConfigFile -eq "s") {
    Remove-Item "$outputFolder\SimuRacerCast.ini" -Force
}

Write-Host "¡Copia finalizada!"

Start-Sleep -s 3